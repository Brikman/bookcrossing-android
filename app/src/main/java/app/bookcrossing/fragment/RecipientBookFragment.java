package app.bookcrossing.fragment;


import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import app.bookcrossing.R;
import app.bookcrossing.async.PictureTask;
import app.bookcrossing.client.net.dto.Book;

@SuppressLint("ValidFragment")
public class RecipientBookFragment extends Fragment {

    private Book book;

    private ImageView picture;

    public RecipientBookFragment(Book book) {
        this.book = book;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle bundle) {
        View view = inflater.inflate(R.layout.fragment_card_recipient_book, container, false);
        picture = view.findViewById(R.id.recipient_book_picture);
        TextView title = view.findViewById(R.id.recipient_book_title);
        TextView author = view.findViewById(R.id.recipient_book_author);
        title.setText(book.getTitle());
        author.setText(book.getAuthor());
        return view;
    }

    public ImageView getBookPicture() {
        return picture;
    }
}
