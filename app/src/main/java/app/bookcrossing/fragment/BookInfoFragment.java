package app.bookcrossing.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.bookcrossing.R;
import app.bookcrossing.adapter.ViewPagerAdapter;
import app.bookcrossing.client.net.dto.Book;

@SuppressLint("ValidFragment")
public class BookInfoFragment extends Fragment implements ViewPagerAdapter.ViewPage {

    private String title;
    private Book book;

    private TextView bookId;
    private TextView description;

    public BookInfoFragment(String title, Book book) {
        this.title = title;
        this.book = book;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = inflater.inflate(R.layout.fragment_book_info, container, false);
        bookId = view.findViewById(R.id.book_id);
        description = view.findViewById(R.id.book_description);
        update(book);
        return view;
    }

    public void update(Book book) {
        this.book = book;
        bookId.setText(String.valueOf(book.getId()));
        if (book.getDescription() == null || book.getDescription().trim().isEmpty()) {
            description.setText(getString(R.string.empty_book_description));
        } else {
            description.setText(book.getDescription());
        }
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public Fragment getFragment() {
        return this;
    }
}
