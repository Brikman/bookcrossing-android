package app.bookcrossing.fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import app.bookcrossing.R;
import app.bookcrossing.activity.UserActivity;
import app.bookcrossing.adapter.ViewPagerAdapter;
import app.bookcrossing.async.AsyncClientTask;
import app.bookcrossing.async.FutureHolder;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.net.dto.Book;
import app.bookcrossing.client.net.dto.User;
import app.bookcrossing.client.net.packet.response.EntityListResponse;
import app.bookcrossing.commons.ExtraKey;

@SuppressLint("ValidFragment")
public class BookCrossingFragment extends Fragment
        implements ViewPagerAdapter.ViewPage, FutureHolder {

    private String title;
    private Book book;
    private boolean lastConfirmed;

    private ProgressBar progressBar;
    private ViewGroup routeContainer;

    private AsyncClientTask<EntityListResponse<User>> ownersTask;

    public BookCrossingFragment(String title, Book book, boolean lastConfirmed) {
        this.title = title;
        this.book = book;
        this.lastConfirmed = lastConfirmed;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = inflater.inflate(R.layout.fragment_book_crossing, container, false);

        progressBar = view.findViewById(R.id.progress);
        routeContainer = view.findViewById(R.id.route_container);

        Client client = Client.getInstance();
        ownersTask = new AsyncClientTask<EntityListResponse<User>>()
                .before(() -> progressBar.setVisibility(View.VISIBLE))
                .forFuture(client.getBookOwners(book.getId()))
                .onSuccess(this::showRoute)
                .after(() -> progressBar.setVisibility(View.GONE));
        ownersTask.execute();

        return view;
    }

    public void showRoute(EntityListResponse<User> response) {
        LayoutInflater inflater = getLayoutInflater();
        List<User> users = response.getEntities();

        Client client = Client.getInstance();
        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            View routeItem = inflater.inflate(R.layout.book_route_item, null);
            TextView text = routeItem.findViewById(R.id.route_item_text);
            ImageView image = routeItem.findViewById(R.id.route_item_icon);
            text.setText(user.getName());
            routeItem.setOnClickListener(v -> {
                Intent intent = new Intent(getActivity(), UserActivity.class);
                intent.putExtra(ExtraKey.USER, user);
                startActivity(intent);
            });

            // TRASH
            if (i == 0) {
                if (user.getId() == client.getClientId()) {
                    if (i == users.size() - 2) {
                        image.setImageResource(R.drawable.route_start_dotted_me);
                    } else {
                        image.setImageResource(R.drawable.route_start_me);
                    }
                } else {
                    if (i == users.size() - 2) {
                        image.setImageResource(R.drawable.route_start_dotted);
                    } else {
                        image.setImageResource(R.drawable.route_start);
                    }
                }
            } else if (i != users.size() - 1) {
                if (user.getId() == client.getClientId()) {
                    if (i == users.size() - 2 && !lastConfirmed) {
                        image.setImageResource(R.drawable.route_mid_dotted_me);
                    } else {
                        image.setImageResource(R.drawable.route_mid_me);
                    }
                } else {
                    if (i == users.size() - 2 && !lastConfirmed) {
                        image.setImageResource(R.drawable.route_mid_dotted);
                    } else {
                        image.setImageResource(R.drawable.route_mid);
                    }
                }
            } else {
                if (user.getId() == client.getClientId()) {
                    if (i == users.size() - 1 && !lastConfirmed) {
                        image.setImageResource(R.drawable.route_end_dotted_me);
                    } else {
                        image.setImageResource(R.drawable.route_end_me);
                    }
                } else {
                    if (i == users.size() - 1 && !lastConfirmed) {
                        image.setImageResource(R.drawable.route_end_dotted);
                    } else {
                        image.setImageResource(R.drawable.route_end);
                    }
                }
            }
            routeContainer.addView(routeItem);
        }
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public Fragment getFragment() {
        return this;
    }

    @Override
    public void cancelFutures() {
        if (ownersTask != null) {
            ownersTask.cancel(true);
        }
    }
}
