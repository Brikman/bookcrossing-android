package app.bookcrossing.fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.bookcrossing.R;
import app.bookcrossing.activity.BookActivity;
import app.bookcrossing.adapter.BookCardListAdapter;
import app.bookcrossing.adapter.ViewPagerAdapter;
import app.bookcrossing.async.AsyncClientTask;
import app.bookcrossing.async.FutureHolder;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.future.ResponseFuture;
import app.bookcrossing.client.net.dto.Book;
import app.bookcrossing.client.net.packet.response.EntityListResponse;
import app.bookcrossing.commons.ExtraKey;

@SuppressLint("ValidFragment")
public class BookListFragment extends Fragment
        implements ViewPagerAdapter.ViewPage, SwipeRefreshLayout.OnRefreshListener, FutureHolder {

    public enum ListView {
        COLLECTION, HISTORY
    }

    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private TextView emptyLabel;

    private ListView listView;
    private String title;

    private AsyncClientTask<EntityListResponse<Book>> bookListTask;

    public BookListFragment(String title, ListView listView) {
        this.listView = listView;
        this.title = title;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = inflater.inflate(R.layout.fragment_book_list, null);

        refreshLayout = view.findViewById(R.id.refresh_layout);
        refreshLayout.setOnRefreshListener(this);

        recyclerView = view.findViewById(R.id.book_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        emptyLabel = view.findViewById(R.id.empty_list_label);

        onRefresh();

        return view;
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(true);
        emptyLabel.setVisibility(View.INVISIBLE);

        Client client = Client.getInstance();
        ResponseFuture<EntityListResponse<Book>> future =
                listView == ListView.COLLECTION ? client.getBookList() : client.getBookHistoryList();
        bookListTask = new AsyncClientTask<EntityListResponse<Book>>()
                .forFuture(future)
                .onSuccess(this::showCollection)
                .after(() -> refreshLayout.setRefreshing(false));
        bookListTask.execute();
    }

    private void showCollection(EntityListResponse<Book> response) {
        List<Book> books = response.getEntities();
        BookCardListAdapter adapter = new BookCardListAdapter(books, R.layout.card_book_compact);
        adapter.setItemOnClickListener(this::onBookClick);
        recyclerView.setAdapter(adapter);
        if (books.size() == 0) {
            emptyLabel.setVisibility(View.VISIBLE);
        }
    }

    public void onBookClick(View view) {
        List<Book> books = getAdapter().getBooks();
        Book book = books.get(recyclerView.getChildLayoutPosition(view));
        Intent intent = new Intent(getActivity(), BookActivity.class);
        intent.putExtra(ExtraKey.BOOK, book);
        if (listView == ListView.COLLECTION)
            intent.putExtra(ExtraKey.BOOK_EDITABLE, true);
        startActivity(intent);
    }

    @Override
    public void cancelFutures() {
        bookListTask.cancel(true);
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter != null) {
            ((BookCardListAdapter) adapter).cancelPictureDownloading();
        }
    }

    public BookCardListAdapter getAdapter() {
        return (BookCardListAdapter) recyclerView.getAdapter();
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public Fragment getFragment() {
        return this;
    }
}
