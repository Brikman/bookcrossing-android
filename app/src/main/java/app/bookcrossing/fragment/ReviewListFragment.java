package app.bookcrossing.fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import app.bookcrossing.R;
import app.bookcrossing.activity.UserActivity;
import app.bookcrossing.adapter.ReviewListAdapter;
import app.bookcrossing.adapter.ViewPagerAdapter;
import app.bookcrossing.async.AsyncClientTask;
import app.bookcrossing.async.FutureHolder;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.net.dto.Book;
import app.bookcrossing.client.net.dto.Review;
import app.bookcrossing.client.net.dto.User;
import app.bookcrossing.client.net.packet.response.EntityResponse;
import app.bookcrossing.client.net.packet.response.ReviewListResponse;
import app.bookcrossing.client.net.packet.response.StatusResponse;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.dialog.ProgressDialog;
import app.bookcrossing.dialog.ReviewDialog;

@SuppressLint("ValidFragment")
public class ReviewListFragment extends Fragment implements ViewPagerAdapter.ViewPage, FutureHolder {

    private String title;
    private Book book;
    private User user;

    private View leaveReviewContainer;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private ReviewListAdapter adapter;

    private ReviewDialog reviewDialog;
    private ProgressDialog progressDialog;

    private AsyncClientTask<ReviewListResponse>   reviewListTask;
    private AsyncClientTask<StatusResponse>       leaveReviewTask;
    private AsyncClientTask<EntityResponse<User>> userTask;

    public ReviewListFragment(String title, Book book) {
        this.title = title;
        this.book = book;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = inflater.inflate(R.layout.fragment_review_list, container, false);
        leaveReviewContainer = view.findViewById(R.id.feedback_container);
        recyclerView = view.findViewById(R.id.review_list);
        progressBar = view.findViewById(R.id.review_progress);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration decoration = new DividerItemDecoration(
                recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(decoration);

        leaveReviewContainer.setVisibility(View.GONE);
        leaveReviewContainer.setOnClickListener(this::onLeaveFeedback);
        progressBar.setVisibility(View.VISIBLE);

        Client client = Client.getInstance();
        reviewListTask = new AsyncClientTask<ReviewListResponse>()
                .forFuture(client.getReviewList(book.getId()))
                .onSuccess(this::showReviews)
                .onRejected(reject -> Toast.makeText(getActivity(), "Не удалось загрузить отзывы", Toast.LENGTH_LONG).show())
                .onException(ex -> Toast.makeText(getActivity(), "Не удалось загрузить отзывы", Toast.LENGTH_LONG).show())
                .after(() -> progressBar.setVisibility(View.GONE));
        reviewListTask.execute();

        userTask = new AsyncClientTask<EntityResponse<User>>()
                .forFuture(client.getUser())
                .onSuccess(response -> user = response.getEntity());
        userTask.execute();

        return view;
    }

    public void showReviews(ReviewListResponse response) {
        List<Review> reviews = response.getReviews();

        Client client = Client.getInstance();
        boolean flag = true;
        for (int i = 0; i < reviews.size(); i++) {
            Review review = reviews.get(i);
            if (review.getUser().getId() == client.getClientId()) {
                flag = false;
                reviews.remove(i);
                reviews.add(0, review);
                break;
            }
        }
        if (flag) {
            leaveReviewContainer.setVisibility(View.VISIBLE);
        }

        int color = getActivity().getResources().getColor(R.color.backgroundLightBlue);

        adapter = new ReviewListAdapter(response.getReviews());
        adapter.setupPrimaryReview(client.getClientId(), color);
        adapter.setOnItemClickListener(this::onReviewClick);
        recyclerView.setAdapter(adapter);
    }

    public void onReviewClick(Review review) {
        Intent intent = new Intent(getActivity(), UserActivity.class);
        intent.putExtra(ExtraKey.USER, review.getUser());
        startActivity(intent);
    }

    public void onLeaveFeedback(View view) {
        reviewDialog = new ReviewDialog();
        reviewDialog.setOnSendReviewListener(this::sendReview);
        reviewDialog.show(getActivity().getFragmentManager(), "review_dialog");
    }

    public void sendReview(String comment, int rating) {
        reviewDialog.dismiss();
        progressDialog = new ProgressDialog(() -> {
            Client client = Client.getInstance();
            leaveReviewTask = new AsyncClientTask<StatusResponse>()
                    .forFuture(client.leaveReview(book.getId(), comment, rating))
                    .onSuccess(response -> {
                        progressDialog.showSuccess("Отзыв успешно сохранён");
                        leaveReviewContainer.setVisibility(View.GONE);
                        if (user != null) {
                            Review review = new Review();
                            review.setUser(user);
                            review.setBook(book);
                            review.setRating(rating);
                            review.setComment(comment);
                            adapter.addReview(0, review);
                        }
                    })
                    .onRejected(rej -> progressDialog.showError("Ошибка"))
                    .onException(ex -> progressDialog.showError("Ошибка"));
            leaveReviewTask.execute();
        }, 1000, "Сохранение...", false);
        progressDialog.show(getActivity().getFragmentManager(), "progress_dialog");
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public Fragment getFragment() {
        return this;
    }

    @Override
    public void cancelFutures() {
        if (reviewListTask != null) {
            reviewListTask.cancel(true);
        }
        if (leaveReviewTask != null) {
            leaveReviewTask.cancel(true);
        }
        if (userTask != null) {
            userTask.cancel(true);
        }
        if (adapter != null) {
            adapter.cancelPictureDownloading();
        }
    }
}
