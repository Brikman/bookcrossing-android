package app.bookcrossing.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.bookcrossing.R;
import app.bookcrossing.activity.IncomingTransferActivity;
import app.bookcrossing.activity.OutboundTransferActivity;
import app.bookcrossing.adapter.TransferCardListAdapter;
import app.bookcrossing.adapter.ViewPagerAdapter;
import app.bookcrossing.async.AsyncClientTask;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.net.dto.Transfer;
import app.bookcrossing.client.net.packet.response.EntityListResponse;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.util.RequestCode;

@SuppressLint("ValidFragment")
public class TransferListFragment extends Fragment
        implements ViewPagerAdapter.ViewPage, SwipeRefreshLayout.OnRefreshListener  {

    public enum TransferView {
        INCOMING, OUTBOUND
    }

    private String title;
    private TransferView view;

    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private TextView emptyLabel;

    private AsyncClientTask<EntityListResponse<Transfer>> transferTask;

    public TransferListFragment(String title, TransferView view) {
        this.title = title;
        this.view = view;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = inflater.inflate(R.layout.fragment_transfer_list, null);
        refreshLayout = view.findViewById(R.id.transfer_list_refresh_layout);
        refreshLayout.setOnRefreshListener(this);
        recyclerView = view.findViewById(R.id.transfer_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        emptyLabel = view.findViewById(R.id.empty_label);
        onRefresh();
        return view;
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(true);
        emptyLabel.setVisibility(View.GONE);

        Client client = Client.getInstance();
        transferTask = new AsyncClientTask<>();
        if (view == TransferView.INCOMING) {
            transferTask.forFuture(client.getIncomingTransferList());
        } else {
            transferTask.forFuture(client.getOutboundTransferList());
        }
        transferTask.onSuccess(this::showTransfers).after(() -> refreshLayout.setRefreshing(false));
        transferTask.execute();
    }

    private void showTransfers(EntityListResponse<Transfer> response) {
        List<Transfer> transfers = response.getEntities();
        TransferCardListAdapter adapter = new TransferCardListAdapter(transfers, view);
        adapter.setItemOnClickListener(this::onTransferClick);
        recyclerView.setAdapter(adapter);
        if (transfers.size() == 0) {
            emptyLabel.setVisibility(View.VISIBLE);
        }
    }

    public void onTransferClick(View view) {
        List<Transfer> transfers = ((TransferCardListAdapter) recyclerView.getAdapter()).getTransfers();
        Transfer transfer = transfers.get(recyclerView.getChildLayoutPosition(view));

        if (this.view == TransferView.INCOMING) {
            Intent intent = new Intent(getActivity(), IncomingTransferActivity.class);
            intent.putExtra(ExtraKey.TRANSFER, transfer);
            startActivityForResult(intent, RequestCode.TRANSFER_CONFIRM);
        } else {
            Intent intent = new Intent(getActivity(), OutboundTransferActivity.class);
            intent.putExtra(ExtraKey.TRANSFER, transfer);
            startActivity(intent);
        }
    }

    public TransferCardListAdapter getTransferAdapter() {
        return (TransferCardListAdapter) recyclerView.getAdapter();
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public Fragment getFragment() {
        return this;
    }
}
