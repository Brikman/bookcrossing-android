package app.bookcrossing.client.codec;


import app.bookcrossing.client.net.packet.Packet;

public interface PacketDecoder<T extends Packet> {

    T decodePacket(Envelope envelope) throws DecodingException;
}
