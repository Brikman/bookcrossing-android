package app.bookcrossing.client.codec;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import app.bookcrossing.client.net.dto.Book;
import app.bookcrossing.client.net.dto.Entity;
import app.bookcrossing.client.net.dto.Transfer;
import app.bookcrossing.client.net.dto.User;
import app.bookcrossing.client.net.packet.response.EntityListResponse;


public class EntityListResponseDeserializer extends JsonDeserializer<EntityListResponse> {

    private static Set<Class<? extends Entity>> entities = new HashSet<>(
            Arrays.asList(User.class, Book.class, Transfer.class));

    @Override
        public EntityListResponse deserialize(JsonParser parser, DeserializationContext context)
            throws IOException {
        ObjectCodec codec = parser.getCodec();
        JsonNode root = codec.readTree(parser);
        Iterator<JsonNode> iterator = root.get("entities").iterator();
        List<Entity> entityList = new ArrayList<>();
        String entityType = root.get("entityType").textValue();
        if (entityType != null) {
            Class<? extends Entity> entityClass = null;
            for (Class<? extends Entity> dtoClass : entities) {
                if (dtoClass.getSimpleName().equalsIgnoreCase(entityType)) {
                    entityClass = dtoClass;
                    break;
                }
            }
            if (entityClass != null) {
                while (iterator.hasNext()) {
                    JsonNode entityNode = iterator.next();
                    entityList.add(codec.treeToValue(entityNode, entityClass));
                }
            } else {
                throw new IOException("cannot determine entity for name: " + entityType);
            }
        }

        long requestId = root.get("id").longValue();
        byte version = (byte) root.get("version").intValue();
        return new EntityListResponse(entityList, requestId, version);
    }
}