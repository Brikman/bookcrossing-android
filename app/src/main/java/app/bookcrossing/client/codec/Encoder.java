package app.bookcrossing.client.codec;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import app.bookcrossing.client.net.packet.Packet;
import app.bookcrossing.logging.TagHelper;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class Encoder extends MessageToByteEncoder<Packet> {

    private static final String TAG = TagHelper.getTag();

    private static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.registerModule(new TimeModule());
    }

    /*
     * Envelope format:
     *  _____________________________HEADER(15b)________________________   _BODY(..)__
     * /                                                                \ /           \
     * | id(8b) | version(1b) | type(1b) | code(1b) | payload_length(4b) | payload(..) |
     */
    @Override
    protected void encode(ChannelHandlerContext context, Packet packet, ByteBuf buffer) throws Exception {
        byte[] payload = mapper.writeValueAsString(packet).getBytes();
        Log.i(TAG, "SENT: " + new String(payload));

        buffer.writeLong(packet.getId());
        buffer.writeByte(packet.getVersion());
        buffer.writeByte(packet.getType());
        buffer.writeByte(packet.getCode());
        buffer.writeInt(payload.length);
        buffer.writeBytes(payload);

        context.flush();
    }
}
