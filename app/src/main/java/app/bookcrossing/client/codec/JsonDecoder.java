package app.bookcrossing.client.codec;


import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.StringReader;
import java.nio.charset.StandardCharsets;

import app.bookcrossing.client.net.packet.Packet;
import app.bookcrossing.logging.TagHelper;

public class JsonDecoder implements PacketDecoder<Packet> {

    private static final String TAG = TagHelper.getTag();
    private static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.registerModule(new TimeModule());
    }

    public Packet decodePacket(Envelope envelope)
            throws DecodingException {
        try {
            String json = new String(envelope.getPayload(), StandardCharsets.UTF_8);
            Log.i(TAG, "RECEIVED JSON: " + (json.length() > 500 ? "<...>" : json));
            return mapper.readValue(new StringReader(json), envelope.getPacketClass());
        } catch (Exception e) {
            throw new DecodingException(e);
        }
    }
}