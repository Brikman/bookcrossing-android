package app.bookcrossing.client.codec;

import android.util.Log;

import java.io.IOException;
import java.util.List;

import app.bookcrossing.client.net.packet.Packet;
import app.bookcrossing.client.net.packet.PacketRegistry;
import app.bookcrossing.client.net.packet.response.PictureResponse;
import app.bookcrossing.logging.TagHelper;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;

public class Decoder extends ReplayingDecoder<Decoder.DecodingState> {

    protected enum DecodingState {
        ID, VERSION, TYPE, CODE, LENGTH, PAYLOAD
    }

    private static final String TAG = TagHelper.getTag();

    private static JsonDecoder jsonDecoder = new JsonDecoder();
    private static PictureDecoder pictureDecoder = new PictureDecoder();

    private Envelope envelope = new Envelope();

    public Decoder() {
        super(DecodingState.values()[0]);
    }

    /*
    * Envelope format:
    *  _____________________________HEADER(15b)________________________   _BODY(..)__
    * /                                                                \ /           \
    * | id(8b) | version(1b) | type(1b) | code(1b) | payload_length(4b) | payload(..) |
    */
    @Override
    protected void decode(ChannelHandlerContext context, ByteBuf buffer, List<Object> result)
            throws Exception {
        switch (state()) {
            case ID:
                envelope.setId(buffer.readLong());
                nextState();
            case VERSION:
                envelope.setVersion(buffer.readByte());
                nextState();
            case TYPE:
                envelope.setType(buffer.readByte());
                nextState();
            case CODE:
                envelope.setCode(buffer.readByte());
                nextState();
            case LENGTH:
                int length = buffer.readInt();
                if (length <= 0)
                    throw new DecodingException("payload length must be positive: " + length);
                envelope.setPayload(new byte[length]);
                nextState();
            case PAYLOAD:
                buffer.readBytes(envelope.getPayload(), 0, envelope.getPayload().length);
                Packet packet = resolve(envelope);
                result.add(packet);
                nextState();
        }
    }

    private Packet resolve(Envelope envelope) throws IOException {
        byte type = envelope.getType();
        byte code = envelope.getCode();
        try {
            Class<? extends Packet> packetClass = PacketRegistry.forType(type).getPacket(code);
            envelope.setPacketClass(packetClass);
            if (packetClass == PictureResponse.class) {
                return pictureDecoder.decodePacket(envelope);
            } else {
                return jsonDecoder.decodePacket(envelope);
            }
        } catch (Exception e) {
            throw new DecodingException(e.getMessage());
        }
    }

    private void nextState() {
        DecodingState[] values = DecodingState.values();
        checkpoint(values[(state().ordinal() + 1) % values.length]);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext context, Throwable cause) throws Exception {
        if (cause instanceof DecodingException) {
            Log.w(TAG, cause.getMessage());
        } else {
            Log.w(TAG, cause.getMessage(), cause);
        }
        checkpoint(DecodingState.VERSION);
    }
}