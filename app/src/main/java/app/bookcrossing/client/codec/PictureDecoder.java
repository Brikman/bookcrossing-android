package app.bookcrossing.client.codec;


import java.nio.ByteBuffer;

import app.bookcrossing.client.net.dto.Picture;
import app.bookcrossing.client.net.packet.response.PictureResponse;
import app.bookcrossing.logging.TagHelper;

public class PictureDecoder implements PacketDecoder<PictureResponse> {

    /*  Payload :
     *    PictureResponse :
     *      long   id       [8] +
     *      byte   code     [1] +
     *      byte   version  [1] +
     *      int    length=x [4] = 14 (bytes)
     *      byte[] picture  [x] = 14 + x (bytes)
     */
    @Override
    public PictureResponse decodePacket(Envelope envelope) throws DecodingException {
        ByteBuffer buffer = ByteBuffer.wrap(envelope.getPayload());
        long id = buffer.getLong();
        byte code = buffer.get();
        byte version = buffer.get();
        int length = buffer.getInt();
        byte[] bytes = new byte[length];
        buffer.get(bytes);

        return new PictureResponse(new Picture(bytes), id, version);
    }
}
