package app.bookcrossing.client.net.dto;


import android.os.Parcel;

import lombok.Data;

@Data
public class Review implements Entity {

    private long id;
    private User user;
    private Book book;
    private String comment;
    private int rating;

    public static final int MAX_RATING = 5;
    public static final int COMMENT_MAX_LENGTH = 500;

    public Review() {}

    protected Review(Parcel in) {
        id = in.readLong();
        user = in.readParcelable(User.class.getClassLoader());
        book = in.readParcelable(Book.class.getClassLoader());
        comment = in.readString();
        rating = in.readInt();
    }

    public static final Creator<Review> CREATOR = new Creator<Review>() {
        @Override
        public Review createFromParcel(Parcel in) {
            return new Review(in);
        }

        @Override
        public Review[] newArray(int size) {
            return new Review[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeParcelable(user, flags);
        dest.writeParcelable(book, flags);
        dest.writeString(comment);
        dest.writeInt(rating);
    }
}
