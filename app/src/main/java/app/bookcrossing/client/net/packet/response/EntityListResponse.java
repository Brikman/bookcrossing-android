package app.bookcrossing.client.net.packet.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import app.bookcrossing.client.codec.EntityListResponseDeserializer;
import app.bookcrossing.client.net.dto.Entity;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = EntityListResponseDeserializer.class)
public class EntityListResponse<T extends Entity> extends Response {

    private List<T> entities;

    @JsonCreator
    public EntityListResponse(@JsonProperty("entities") Collection<T> entities,
                              @JsonProperty("requestId") long requestId,
                              @JsonProperty("version") byte version) {
        super(requestId, version);
        this.entities = new ArrayList<>(entities);
    }
}
