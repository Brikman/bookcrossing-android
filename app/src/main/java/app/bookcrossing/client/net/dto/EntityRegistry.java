package app.bookcrossing.client.net.dto;


import java.util.HashMap;
import java.util.Map;

public class EntityRegistry {

    private static final HashMap<String,Class<? extends Entity>> registry
            = new HashMap<String, Class<? extends Entity>>() {{
        put("user", User.class);
        put("book", Book.class);
        put("address", Address.class);
        put("picture", Picture.class);
        put("transfer", Transfer.class);
    }};

    public static String getKey(Entity entity) {
        for (Map.Entry<String, Class<? extends Entity>> entry : registry.entrySet()) {
            if (entry.getValue() == entity.getClass()) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static Class<? extends Entity> getEntity(String key) {
        return registry.get(key);
    }
}
