package app.bookcrossing.client.net.packet.request;

import lombok.Data;

@Data
public class RegistrationRequest extends Request {

    private String name;
    private String email;
    private String password;

    public RegistrationRequest(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }
}
