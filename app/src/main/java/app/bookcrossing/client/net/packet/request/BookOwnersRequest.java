package app.bookcrossing.client.net.packet.request;


import lombok.Data;

@Data
public class BookOwnersRequest extends Request {

    private long bookId;

    public BookOwnersRequest(long bookId) {
        this.bookId = bookId;
    }
}
