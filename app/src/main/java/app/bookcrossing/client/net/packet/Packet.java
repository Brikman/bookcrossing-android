package app.bookcrossing.client.net.packet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

import app.bookcrossing.client.net.packet.request.Request;
import app.bookcrossing.client.net.packet.response.Response;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class Packet implements Serializable {

    private long id;
    private byte code;
    private byte version;

    protected Packet(long id) {
        this(id, (byte) 1);
    }

    protected Packet(long id, byte version) {
        this.id = id;
        this.version = version;
        this.code = PacketRegistry.getPacketCode(this);
    }

    @JsonIgnore
    public byte getType() {
        if (this instanceof Request)
            return 1;
        if (this instanceof Response)
            return 2;
        return 1;
    }
}
