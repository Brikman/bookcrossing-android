package app.bookcrossing.client.net.packet.request;


import app.bookcrossing.client.net.dto.Book;
import lombok.Data;

@Data
public class EditBookRequest extends Request {

    private Book book;

    public EditBookRequest(Book book) {
        this.book = book;
    }
}
