package app.bookcrossing.client.net.dto;

import android.os.Parcelable;

public interface Entity extends Parcelable {
    long getId();
}
