package app.bookcrossing.client.net.packet.request;

import app.bookcrossing.client.net.dto.Transfer;
import lombok.Data;

@Data
public class EditTransferRequest extends Request {

    private Transfer transfer;

    public EditTransferRequest(Transfer transfer) {
        this.transfer = transfer;
    }
}
