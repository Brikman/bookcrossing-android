package app.bookcrossing.client.net.packet.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StatusResponse extends Response {

    private boolean status;
    private String  message;

    @JsonCreator
    public StatusResponse(@JsonProperty("status") boolean status,
                          @JsonProperty("message") String message,
                          @JsonProperty("requestId") long requestId,
                          @JsonProperty("version") byte version) {
        super(requestId, version);
        this.status = status;
        this.message = message;
    }

    @JsonProperty("status")
    public boolean isSuccess() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "StatusResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                '}';
    }
}
