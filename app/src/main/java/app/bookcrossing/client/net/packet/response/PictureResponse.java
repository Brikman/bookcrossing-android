package app.bookcrossing.client.net.packet.response;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import app.bookcrossing.client.net.dto.Picture;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PictureResponse extends Response {

    private Picture picture;

    @JsonCreator
    public PictureResponse(@JsonProperty("picture") Picture picture,
                           @JsonProperty("requestId") long requestId,
                           @JsonProperty("version") byte version) {
        super(requestId, version);
        this.picture = picture;
    }
}
