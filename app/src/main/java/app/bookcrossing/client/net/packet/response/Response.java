package app.bookcrossing.client.net.packet.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import app.bookcrossing.client.net.packet.Packet;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class Response extends Packet {

    protected Response(long requestId, byte version) {
        super(requestId, version);
    }
}
