package app.bookcrossing.client.net.packet.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RejectionPacket extends Response {

    private final String message;

    @JsonCreator
    public RejectionPacket(@JsonProperty("message") String message,
                           @JsonProperty("requestId") long requestId,
                           @JsonProperty("version") byte version) {
        super(requestId, version);
        this.message = message;
    }
}
