package app.bookcrossing.client.net.packet.response;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import app.bookcrossing.client.net.dto.Book;
import app.bookcrossing.client.net.dto.Review;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReviewListResponse extends Response {

    private Book book;
    private List<Review> reviews;

    @JsonCreator
    public ReviewListResponse(@JsonProperty("book") Book book,
                              @JsonProperty("entities") List<Review> reviews,
                              @JsonProperty("requestId") long requestId,
                              @JsonProperty("version") byte version) {
        super(requestId, version);
        this.book = book;
        this.reviews = reviews;
    }
}
