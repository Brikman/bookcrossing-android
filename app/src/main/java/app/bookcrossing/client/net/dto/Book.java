package app.bookcrossing.client.net.dto;

import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Book implements Entity {

    private long id;
    private String title;
    private String author;
    private String description;
    private Picture picture;
    private float rating;

    public static final int TITLE_MAX_LENGTH = 100;
    public static final int DESCRIPTION_MAX_LENGTH = 1000;
    public static final int AUTHORS_MAX_COUNT = 4;

    public Book() {}

    protected Book(Parcel parcel) {
        id = parcel.readLong();
        title = parcel.readString();
        author = parcel.readString();
        description = parcel.readString();
        picture = parcel.readParcelable(Picture.class.getClassLoader());
        rating = parcel.readFloat();
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeLong(id);
        parcel.writeString(title);
        parcel.writeString(author);
        parcel.writeString(description);
        parcel.writeParcelable(picture, flags);
        parcel.writeFloat(rating);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel parcel) {
            return new Book(parcel);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };
}
