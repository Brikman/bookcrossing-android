package app.bookcrossing.client.net.packet.request;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.concurrent.atomic.AtomicLong;

import app.bookcrossing.client.net.packet.Packet;


public abstract class Request extends Packet {

    @JsonIgnore
    private static AtomicLong counter = new AtomicLong(0);

    protected Request() {
        super(counter.incrementAndGet());
    }
}
