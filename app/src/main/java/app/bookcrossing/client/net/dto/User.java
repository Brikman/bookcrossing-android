package app.bookcrossing.client.net.dto;

import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Entity {

    private long id;
    private String name;
    private String email;
    private String phone;
    private Address address;
    private String about;
    private Picture picture;

    public static final int NAME_MAX_LENGTH = 50;
    public static final int EMAIL_MAX_LENGTH = 100;
    public static final int PHONE_MAX_LENGTH = 20;
    public static final int ABOUT_MAX_LENGTH = 500;

    public User() {}

    protected User(Parcel parcel) {
        id = parcel.readLong();
        name = parcel.readString();
        email = parcel.readString();
        phone = parcel.readString();
        address = parcel.readParcelable(Address.class.getClassLoader());
        about = parcel.readString();
        picture = parcel.readParcelable(Picture.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeString(email);
        parcel.writeString(phone);
        parcel.writeParcelable(address, flags);
        parcel.writeString(about);
        parcel.writeParcelable(picture, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel parcel) {
            return new User(parcel);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
