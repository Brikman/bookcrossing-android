package app.bookcrossing.client.net.packet.request;

import lombok.Data;

@Data
public class LeaveReviewRequest extends Request {

    private long bookId;
    private String comment;
    private int rating;

    public LeaveReviewRequest(long bookId, String comment, int rating) {
        this.bookId = bookId;
        this.comment = comment;
        this.rating = rating;
    }
}