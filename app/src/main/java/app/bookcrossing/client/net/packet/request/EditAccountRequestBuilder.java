package app.bookcrossing.client.net.packet.request;


import java.util.HashMap;
import java.util.Map;

import app.bookcrossing.client.net.dto.Address;
import app.bookcrossing.client.net.dto.Picture;

import static app.bookcrossing.client.net.packet.request.EditAccountRequestBuilder.Field.*;

public class EditAccountRequestBuilder {

    private Map<String, Object> fields = new HashMap<>();

    public EditAccountRequestBuilder setName(String name) {
        fields.put(NAME.getName(), trimToNull(name));
        return this;
    }

    public EditAccountRequestBuilder setEmail(String email) {
        fields.put(EMAIL.getName(), trimToNull(email));
        return this;
    }

    public EditAccountRequestBuilder setPhone(String phone) {
        fields.put(PHONE.getName(), trimToNull(phone));
        return this;
    }

    public EditAccountRequestBuilder setAddress(Address address) {
        fields.put(ADDRESS.getName(), address);
        return this;
    }

    public EditAccountRequestBuilder setAbout(String about) {
        fields.put(ABOUT.getName(), trimToNull(about));
        return this;
    }

    public EditAccountRequestBuilder setPicture(Picture picture) {
        fields.put(PICTURE.getName(), picture);
        return this;
    }

    public EditAccountRequest build() {
        return new EditAccountRequest(fields);
    }

    public boolean isChanged() {
        return !fields.isEmpty();
    }

    private String trimToNull(String str) {
        if (str == null || str.trim().isEmpty()) {
            return null;
        }
        return str.trim();
    }

    enum Field {

        NAME("name"),
        EMAIL("email"),
        PHONE("phone"),
        ADDRESS("address"),
        ABOUT("about"),
        PICTURE("picture");
        String name;

        Field(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}