package app.bookcrossing.client.net.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Address implements Entity {

    private String countryCode;
    private String countryName;
    private String cityCode;
    private String cityName;
    private String street;
    private String house;
    private String apartment;
    private String postalCode;

    public Address() {}

    protected Address(Parcel in) {
        countryCode = in.readString();
        countryName = in.readString();
        cityCode = in.readString();
        cityName = in.readString();
        street = in.readString();
        house = in.readString();
        apartment = in.readString();
        postalCode = in.readString();
    }

    public static final Creator<Address> CREATOR = new Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(countryCode);
        dest.writeString(countryName);
        dest.writeString(cityCode);
        dest.writeString(cityName);
        dest.writeString(street);
        dest.writeString(house);
        dest.writeString(apartment);
        dest.writeString(postalCode);
    }

    @Override
    public long getId() {
        return 0;
    }

    public String toStringCompact() {
        return new StringBuilder().append(countryName).append(", ").append(cityName).toString();
    }

    private String orEmpty(String field, String postfix) {
        return field == null || field.trim().isEmpty() ? "" : field.trim() + postfix;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(orEmpty(countryName, ", "))
                .append(orEmpty(cityName, ", "))
                .append(orEmpty(street, ", "))
                .append(orEmpty(house, ", "))
                .append(orEmpty(apartment, ", "))
                .append(orEmpty(postalCode, ""))
                .toString();
    }
}
