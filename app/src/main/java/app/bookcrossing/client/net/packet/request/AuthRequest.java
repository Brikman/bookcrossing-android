package app.bookcrossing.client.net.packet.request;

import lombok.Data;

@Data
public class AuthRequest extends Request {

    private String email;
    private String password;

    public AuthRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
