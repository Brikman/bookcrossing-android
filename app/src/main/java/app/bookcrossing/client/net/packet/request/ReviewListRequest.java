package app.bookcrossing.client.net.packet.request;


import lombok.Data;

@Data
public class ReviewListRequest extends Request {

    private long bookId;

    public ReviewListRequest(long bookId) {
        this.bookId = bookId;
    }
}
