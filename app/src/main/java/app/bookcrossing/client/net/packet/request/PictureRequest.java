package app.bookcrossing.client.net.packet.request;

import app.bookcrossing.client.net.dto.Entity;
import app.bookcrossing.client.net.dto.EntityRegistry;
import lombok.Data;

@Data
public class PictureRequest extends Request {

    private String entityName;
    private long entityId;

    public PictureRequest(Entity entity) {
        entityId = entity.getId();
        entityName = EntityRegistry.getKey(entity);
    }
}
