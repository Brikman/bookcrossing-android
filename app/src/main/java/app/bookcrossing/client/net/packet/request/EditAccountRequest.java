package app.bookcrossing.client.net.packet.request;

import java.util.Map;

import lombok.Data;

@Data
public class EditAccountRequest extends Request {

    private Map<String, Object> fields;

    EditAccountRequest(Map<String, Object> fields) {
        this.fields = fields;
    }
}
