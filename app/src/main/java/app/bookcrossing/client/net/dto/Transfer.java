package app.bookcrossing.client.net.dto;

import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.joda.time.Instant;

import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transfer implements Entity {

    private long id;
    private User sender;
    private User recipient;
    private Book book;
    private Instant sent;
    private String message;
    private String trackNumber;

    public static final int MESSAGE_MAX_LENGTH = 500;

    public Transfer() {}

    protected Transfer(Parcel parcel) {
        id = parcel.readLong();
        sender = parcel.readParcelable(User.class.getClassLoader());
        recipient = parcel.readParcelable(User.class.getClassLoader());
        book = parcel.readParcelable(Book.class.getClassLoader());
        sent = Instant.parse(parcel.readString());
        message = parcel.readString();
        trackNumber = parcel.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeLong(id);
        parcel.writeParcelable(sender, flags);
        parcel.writeParcelable(recipient, flags);
        parcel.writeParcelable(book, flags);
        parcel.writeString(sent.toString());
        parcel.writeString(message);
        parcel.writeString(trackNumber);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Transfer> CREATOR = new Creator<Transfer>() {
        @Override
        public Transfer createFromParcel(Parcel in) {
            return new Transfer(in);
        }

        @Override
        public Transfer[] newArray(int size) {
            return new Transfer[size];
        }
    };
}
