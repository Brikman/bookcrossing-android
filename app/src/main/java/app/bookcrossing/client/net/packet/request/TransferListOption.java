package app.bookcrossing.client.net.packet.request;

public enum TransferListOption {
    OUTBOUND, INCOMING
}
