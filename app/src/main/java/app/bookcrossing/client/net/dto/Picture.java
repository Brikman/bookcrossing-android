package app.bookcrossing.client.net.dto;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Picture implements Entity {

    private byte[] bytes;

    public Picture(byte[] bytes) {
        this.bytes = bytes;
    }

    @JsonIgnore
    public Bitmap getBitmap() {
        if (bytes == null || bytes.length == 0)
            return null;
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    protected Picture(Parcel in) {
        bytes = in.createByteArray();
    }

    public static final Creator<Picture> CREATOR = new Creator<Picture>() {
        @Override
        public Picture createFromParcel(Parcel in) {
            return new Picture(in);
        }

        @Override
        public Picture[] newArray(int size) {
            return new Picture[size];
        }
    };

    @Override
    public long getId() {
        return 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByteArray(bytes);
    }
}
