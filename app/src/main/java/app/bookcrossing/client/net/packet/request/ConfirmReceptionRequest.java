package app.bookcrossing.client.net.packet.request;

import lombok.Data;

@Data
public class ConfirmReceptionRequest extends Request {

    private long transferId;
    private String message;

    public ConfirmReceptionRequest(long transferId, String message) {
        this.transferId = transferId;
        this.message = message;
    }
}
