package app.bookcrossing.client.net.packet.request;

public enum SendOption {
    SEND_NEW, FORWARD
}
