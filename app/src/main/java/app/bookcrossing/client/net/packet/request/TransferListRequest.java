package app.bookcrossing.client.net.packet.request;

import lombok.Data;

@Data
public class TransferListRequest extends Request {

    private TransferListOption option;

    public TransferListRequest(TransferListOption option) {
        this.option = option;
    }
}
