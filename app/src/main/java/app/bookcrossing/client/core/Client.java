package app.bookcrossing.client.core;

import android.util.Log;

import java.net.InetSocketAddress;

import app.bookcrossing.AppConfig;
import app.bookcrossing.client.future.ActionFuture;
import app.bookcrossing.client.future.ResponseFuture;
import app.bookcrossing.client.future.ResponseFutureListener;
import app.bookcrossing.client.net.dto.Book;
import app.bookcrossing.client.net.dto.Entity;
import app.bookcrossing.client.net.dto.Transfer;
import app.bookcrossing.client.net.dto.User;
import app.bookcrossing.client.net.packet.request.AuthRequest;
import app.bookcrossing.client.net.packet.request.BookListRequest;
import app.bookcrossing.client.net.packet.request.BookOwnersRequest;
import app.bookcrossing.client.net.packet.request.ConfirmReceptionRequest;
import app.bookcrossing.client.net.packet.request.EditAccountRequest;
import app.bookcrossing.client.net.packet.request.EditBookRequest;
import app.bookcrossing.client.net.packet.request.EditTransferRequest;
import app.bookcrossing.client.net.packet.request.EntityRequest;
import app.bookcrossing.client.net.packet.request.LeaveReviewRequest;
import app.bookcrossing.client.net.packet.request.PictureRequest;
import app.bookcrossing.client.net.packet.request.RecipientListRequest;
import app.bookcrossing.client.net.packet.request.RegistrationRequest;
import app.bookcrossing.client.net.packet.request.ReviewListRequest;
import app.bookcrossing.client.net.packet.request.SendBookRequest;
import app.bookcrossing.client.net.packet.request.SendOption;
import app.bookcrossing.client.net.packet.request.TransferListOption;
import app.bookcrossing.client.net.packet.request.TransferListRequest;
import app.bookcrossing.client.net.packet.response.EntityListResponse;
import app.bookcrossing.client.net.packet.response.EntityResponse;
import app.bookcrossing.client.net.packet.response.PictureResponse;
import app.bookcrossing.client.net.packet.response.RecipientListResponse;
import app.bookcrossing.client.net.packet.response.ReviewListResponse;
import app.bookcrossing.client.net.packet.response.StatusResponse;
import app.bookcrossing.logging.TagHelper;

public class Client {

    private static final String TAG = TagHelper.getTag();

    private InetSocketAddress serverAddress = new InetSocketAddress(AppConfig.NETWORK, 9000);
    private Connection connection;

    private Long clientId;
    private boolean autoflush = false;

    private static Client instance;

    private Client() {}

    public static Client getInstance() {
        if (instance == null)
            instance = new Client();
        return instance;
    }

    public void connect() {
        if (!isConnected()) {
            connection = Connection.connect(serverAddress);
            connection.setAutoflush(autoflush);
        }
    }

    public ResponseFuture<EntityResponse<User>> login(String login, String password) {
        Log.i(TAG, "login(email:" + login + ", password:" + password + ")");
        connect();
        AuthRequest request = new AuthRequest(login, password);
        ResponseFuture<EntityResponse<User>> future = connection.send(request);
        future.addListener(new ResponseFutureListener<EntityResponse<User>>() {
            @Override
            public void accepted(ResponseFuture<EntityResponse<User>> future) {
                try {
                    clientId = future.get().getEntity().getId();
                } catch (Exception ignored) {}
            }
        });
        return future;
    }

    public ResponseFuture<EntityResponse<User>> register(String email, String password, String name) {
        Log.i(TAG, "register(name:" + name + ", email:" + email + ", password:" + password + ")");
        connect();
        RegistrationRequest request = new RegistrationRequest(name, email, password);
        ResponseFuture<EntityResponse<User>> future = connection.send(request);
        future.addListener(new ResponseFutureListener<EntityResponse<User>>() {
            @Override
            public void accepted(ResponseFuture<EntityResponse<User>> future) {
                try {
                    clientId = future.get().getEntity().getId();
                } catch (Exception ignored) {}
            }
        });
        return future;
    }

    public ResponseFuture<StatusResponse> editAccount(EditAccountRequest request) {
        Log.i(TAG, "editAccount(...)");
        return connection.send(request);
    }

    public ResponseFuture<EntityResponse<User>> getUser() {
        return getUser(clientId);
    }

    public ResponseFuture<EntityResponse<User>> getUser(long userId) {
        Log.i(TAG, "getUser(id: " + userId + ")");
        EntityRequest request = new EntityRequest(userId, EntityRequest.USER);
        return connection.send(request);
    }

    public ResponseFuture<EntityResponse<Book>> getBook(long bookId) {
        Log.i(TAG, "getBook(id: " + bookId + ")");
        EntityRequest request = new EntityRequest(bookId, EntityRequest.BOOK);
        return connection.send(request);
    }

    public ResponseFuture<EntityListResponse<Book>> getBookHistoryList() {
        Log.i(TAG, "getBookHistoryListByUser");
        BookListRequest request = BookListRequest.history();
        return connection.send(request);
    }

    public ResponseFuture<EntityListResponse<Book>> getBookList() {
        return getBookListByUser(clientId);
    }

    public ResponseFuture<EntityListResponse<Book>> getBookListByUser(long userId) {
        Log.i(TAG, "getBookListByUser(id: " + userId + ")");
        BookListRequest request = BookListRequest.byUser(userId);
        return connection.send(request);
    }

    public ResponseFuture<EntityListResponse<Book>> getBookListByTitle(String title) {
        Log.i(TAG, "getBookListByTitle(title: " + title + ")");
        BookListRequest request = BookListRequest.byTitle(title);
        return connection.send(request);
    }

    public ResponseFuture<StatusResponse> editBook(Book book) {
        Log.i(TAG, "editBook");
        EditBookRequest request = new EditBookRequest(book);
        return connection.send(request);
    }

    public ResponseFuture<PictureResponse> getPicture(Entity entity) {
        Log.i(TAG, "getPicture(entity: " + entity.getClass().getSimpleName() + ")");
        PictureRequest request = new PictureRequest(entity);
        return connection.send(request);
    }

    public ResponseFuture<RecipientListResponse> requireRecipients() {
        Log.i(TAG, "requireRecipients");
        RecipientListRequest request = RecipientListRequest.forNewList();
        return connection.send(request);
    }

    public ResponseFuture<RecipientListResponse> getRecipientsList() {
        Log.i(TAG, "getRecipientsList");
        RecipientListRequest request = RecipientListRequest.forPendingList();
        return connection.send(request);
    }

    public ResponseFuture<StatusResponse> sendBook(Book book, long recipientId, String message, String trackNumber) {
        Log.i(TAG, "sendBook(book:" + book.getTitle() + " to:" + recipientId + ")");
        SendBookRequest request = new SendBookRequest(book, recipientId, SendOption.SEND_NEW, message, trackNumber);
        return connection.send(request);
    }

    public ResponseFuture<StatusResponse> sendBook(long bookId, long recipientId, String message, String trackNumber) {
        Log.i(TAG, "sendBook(bookId:" + bookId + " to:" + recipientId + ")");
        Book book = new Book();
        book.setId(bookId);
        SendBookRequest request = new SendBookRequest(book, recipientId, SendOption.FORWARD, message, trackNumber);
        return connection.send(request);
    }

    public ResponseFuture<EntityListResponse<Transfer>> getOutboundTransferList() {
        Log.i(TAG, "getOutboundTransferList(id: " + clientId + ")");
        TransferListRequest request = new TransferListRequest(TransferListOption.OUTBOUND);
        return connection.send(request);
    }

    public ResponseFuture<EntityListResponse<Transfer>> getIncomingTransferList() {
        Log.i(TAG, "getIncomingTransferList(id: " + clientId + ")");
        TransferListRequest request = new TransferListRequest(TransferListOption.INCOMING);
        return connection.send(request);
    }

    public ResponseFuture<StatusResponse> confirmTransfer(long transferId, String message) {
        Log.i(TAG, "confirmTransfer(id: " + transferId + ")");
        ConfirmReceptionRequest request = new ConfirmReceptionRequest(transferId, message);
        return connection.send(request);
    }

    public ResponseFuture<StatusResponse> editTransfer(Transfer transfer) {
        Log.i(TAG, "editTransfer");
        EditTransferRequest request = new EditTransferRequest(transfer);
        return connection.send(request);
    }

    public ResponseFuture<EntityListResponse<User>> getBookOwners(long bookId) {
        Log.i(TAG, "bookOwnersRequest(id: " + bookId + ")");
        BookOwnersRequest request = new BookOwnersRequest(bookId);
        return connection.send(request);
    }

    public ResponseFuture<StatusResponse> leaveReview(long bookId, String comment, int rating) {
        Log.i(TAG, "leaveReview(id:" + bookId + " comment:" + comment + " rating:" + rating + ")");
        LeaveReviewRequest request = new LeaveReviewRequest(bookId, comment, rating);
        return connection.send(request);
    }

    public ResponseFuture<ReviewListResponse> getReviewList(long bookId) {
        Log.i(TAG, "getReviewList(id:" + bookId + ")");
        ReviewListRequest request = new ReviewListRequest(bookId);
        return connection.send(request);
    }

    public Long getClientId() {
        return clientId;
    }

    public void setAutoflush(boolean flag) {
        this.autoflush = flag;
        connection.setAutoflush(flag);
    }

    public void flush() {
        connection.flush();
    }

    public boolean isConnected() {
        return connection != null && connection.isOpen();
    }

    public void logout() {
        Log.i(TAG, "Logging out...");
        connection.close();
    }

    public ActionFuture logoutFuture() {
        return this::logout;
    }
}
