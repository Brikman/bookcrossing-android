package app.bookcrossing.client.core;

import android.util.Log;

import java.net.SocketAddress;
import java.util.LinkedList;
import java.util.Queue;

import app.bookcrossing.client.ConnectionException;
import app.bookcrossing.client.codec.Decoder;
import app.bookcrossing.client.codec.Encoder;
import app.bookcrossing.client.future.FutureRegistry;
import app.bookcrossing.client.future.ResponseFuture;
import app.bookcrossing.client.net.packet.request.Request;
import app.bookcrossing.client.net.packet.response.Response;
import app.bookcrossing.logging.TagHelper;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelException;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.NonNull;

public class Connection {

    private static final String TAG = TagHelper.getTag();

    private Channel channel;
    private Queue<Request> requestQueue = new LinkedList<>();
    private FutureRegistry futureRegistry;

    private boolean autoflush = true;

    private Connection(Channel channel, FutureRegistry futureRegistry) {
        this.channel = channel;
        this.futureRegistry = futureRegistry;
    }

    public static Connection connect(SocketAddress serverAddress) {
        try {
            Log.i(TAG, "Initializing connection");
            NioEventLoopGroup workerGroup = new NioEventLoopGroup();
            FutureRegistry futureRegistry = new FutureRegistry();
            Bootstrap bootstrap = new Bootstrap()
                    .channel(NioSocketChannel.class)
                    .group(workerGroup)
                    .remoteAddress(serverAddress)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel channel) throws Exception {
                            channel.pipeline()
                                    .addLast(new Encoder())
                                    .addLast(new Decoder())
                                    .addLast(new ClientHandler(futureRegistry));
                        }
                    });
            Channel channel = bootstrap.connect().sync().channel();
            Log.i(TAG, "Connected to: " + channel.remoteAddress());
            return new Connection(channel, futureRegistry);
        } catch (Exception e) {
            Log.e(TAG, "Connection failed: " + e.getMessage());
            throw new ConnectionException(e);
        }
    }

    public ResponseFuture send(@NonNull Request request) {
        if (channel == null)
            throw new ConnectionException("Connection has not been established");
        if (!isOpen())
            throw new ChannelException("Connection closed");
        request.setVersion((byte) 1);
        if (autoflush) {
            channel.writeAndFlush(request);
        } else {
            requestQueue.offer(request);
        }
        return futureRegistry.futureFor(request, this);
    }

    public void flush() {
        if (isOpen()) {
            while (!requestQueue.isEmpty()) {
                Request request = requestQueue.poll();
                channel.writeAndFlush(request);
            }
        }
    }

    public void force(ResponseFuture future) {
        Request request = futureRegistry.getRequest(future);
        if (request != null && requestQueue.remove(request)) {
            channel.writeAndFlush(request);
        }
    }

    public void close() {
        Log.i(TAG, "Closing connection...");
        requestQueue.clear();
        if (channel != null) {
            Log.i(TAG, "Shutting down event loop group...");
            channel.eventLoop().parent().shutdownGracefully();
            Log.i(TAG, "Closing channel...");
            channel.close();
        }
    }

    public boolean isOpen() {
        return channel != null && channel.isOpen();
    }

    public void setAutoflush(boolean flag) {
        this.autoflush = flag;
    }
}
