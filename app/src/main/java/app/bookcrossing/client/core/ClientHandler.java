package app.bookcrossing.client.core;

import android.util.Log;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import app.bookcrossing.client.future.FutureRegistry;
import app.bookcrossing.client.net.packet.response.Response;
import app.bookcrossing.logging.TagHelper;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

class ClientHandler extends ChannelInboundHandlerAdapter {
    
    private static final String TAG = TagHelper.getTag();
    
    private ExecutorService executor = Executors.newSingleThreadExecutor();

    private FutureRegistry futures;

    public ClientHandler(FutureRegistry futures) {
        this.futures = futures;
    }

    @Override
    public void channelRead(ChannelHandlerContext context, Object msg) {
        Log.i(TAG, "Message received: " + msg.getClass().getSimpleName());
        if (msg instanceof Response) {
            executor.execute(() -> futures.notifyFuture((Response) msg));
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext context, Throwable cause) {
        Log.e(TAG, "Exception caught", cause);
        context.close();
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext context) throws Exception {
        List<Runnable> unexecuted = executor.shutdownNow();
        Log.i(TAG, "Channel unregistered" + (!unexecuted.isEmpty() ? ": " + unexecuted.size() + " unexecuted" : ""));
        super.channelUnregistered(context);
    }
}
