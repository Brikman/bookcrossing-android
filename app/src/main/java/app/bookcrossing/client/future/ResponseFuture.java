package app.bookcrossing.client.future;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import app.bookcrossing.client.core.Connection;
import app.bookcrossing.client.net.packet.request.Request;
import app.bookcrossing.client.net.packet.response.RejectionPacket;
import app.bookcrossing.client.net.packet.response.Response;


public class ResponseFuture<T extends Response> {

    private static final long DEFAULT_TIMEOUT_MS = 300_000;

    private final FutureRegistry registry;
    private Connection connection;
    private final CountDownLatch latch = new CountDownLatch(1);

    private volatile T value;
    private boolean isCancelled = false;

    private boolean isDone      = false;
    private ResponseFutureListener<T> listener;

    ResponseFuture(FutureRegistry registry, Connection connection) {
        this.registry = registry;
        this.connection = connection;
    }

    public boolean cancel() {
        if (!isDone) {
            isCancelled = true;
            registry.release(this);
            latch.countDown();
            return true;
        }
        return false;
    }

    public boolean isCancelled() {
        return isCancelled;
    }

    public boolean isDone() {
        return latch.getCount() == 0 && !isCancelled;
    }

    public T get() throws InterruptedException, RejectionException, TimeoutException {
        connection.force(this);
        if (latch.await(DEFAULT_TIMEOUT_MS, TimeUnit.MILLISECONDS)) {
            if (value instanceof RejectionPacket)
                throw new RejectionException((RejectionPacket) value);
            return value;
        }
        throw new TimeoutException();
    }

    public void addListener(ResponseFutureListener<T> listener) {
        this.listener = listener;
    }

    void put(T value) {
        this.value = value;
        isDone = true;
        latch.countDown();
        if (listener != null) {
            listener.accepted(this);
        }
    }
}
