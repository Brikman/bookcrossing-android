package app.bookcrossing.client.future;


import app.bookcrossing.client.net.packet.response.Response;

public interface ResponseFutureListener<T extends Response> {
    void accepted(ResponseFuture<T> future);
}
