package app.bookcrossing.client.future;


import app.bookcrossing.client.net.packet.response.RejectionPacket;

public class RejectionException extends Exception {
    private final RejectionPacket packet;

    public RejectionException(RejectionPacket packet) {
        this.packet = packet;
    }

    public RejectionPacket getPacket() {
        return packet;
    }
}
