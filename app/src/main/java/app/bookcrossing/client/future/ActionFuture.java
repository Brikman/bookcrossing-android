package app.bookcrossing.client.future;

@FunctionalInterface
public interface ActionFuture {

    void execute();
}
