package app.bookcrossing.adapter;


import android.support.annotation.ColorInt;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.bookcrossing.R;
import app.bookcrossing.async.PictureTask;
import app.bookcrossing.client.net.dto.Review;

public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.ReviewViewHolder> {

    private List<Review> reviews;
    private long primaryId;
    private int primaryColor;

    private List<PictureTask> pictureTasks = new ArrayList<>();

    private OnReviewHeaderClickListener listener;


    public ReviewListAdapter(List<Review> reviews) {
        this.reviews = reviews;
    }

    public void setupPrimaryReview(long userId, @ColorInt int color) {
        this.primaryId = userId;
        this.primaryColor = color;
    }

    public void setOnItemClickListener(OnReviewHeaderClickListener listener) {
        this.listener = listener;
    }

    public void addReview(int position, Review review) {
        reviews.add(position, review);
        notifyItemChanged(position);
    }

    public List<Review> getReviews() {
        return reviews;
    }

    @Override
    public ReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_list_item, parent, false);
        return new ReviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ReviewViewHolder holder, int position) {
        Review review = reviews.get(position);
        holder.name.setText(review.getUser().getName());
        holder.rating.setText(String.valueOf(review.getRating()));
        holder.comment.setText(review.getComment());
        if (listener != null) {
            holder.header.setOnClickListener(v -> listener.click(review));
        }
        if (review.getUser().getId() == primaryId) {
            holder.container.setBackgroundColor(primaryColor);
            holder.headerFiller.setVisibility(View.VISIBLE);
        }
        PictureTask pictureTask = new PictureTask(review.getUser(), holder.picture, R.drawable.unknown_person);
        pictureTask.execute();
        pictureTasks.add(pictureTask);
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }

    public void cancelPictureDownloading() {
        for (PictureTask task : pictureTasks) {
            task.cancel(true);
        }
        pictureTasks.clear();
    }

    public class ReviewViewHolder extends RecyclerView.ViewHolder {

        View      container;
        View      headerFiller;
        View      header;
        ImageView picture;
        TextView  name;
        TextView  rating;
        TextView  comment;

        public ReviewViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.review_container);
            headerFiller = itemView.findViewById(R.id.review_header_filler);
            header = itemView.findViewById(R.id.review_header);
            picture = itemView.findViewById(R.id.review_picture);
            name = itemView.findViewById(R.id.review_name);
            rating = itemView.findViewById(R.id.review_rating);
            comment = itemView.findViewById(R.id.review_comment);
        }
    }

    public interface OnReviewHeaderClickListener {

        void click(Review review);
    }
}
