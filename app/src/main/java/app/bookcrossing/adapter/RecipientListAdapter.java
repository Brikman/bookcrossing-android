package app.bookcrossing.adapter;


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.bookcrossing.R;
import app.bookcrossing.async.PictureTask;
import app.bookcrossing.client.net.dto.User;

import static android.support.v7.widget.RecyclerView.ViewHolder;

public class RecipientListAdapter
        extends RecyclerView.Adapter<RecipientListAdapter.RecipientViewHolder> {

    private List<User> recipients;
    private View.OnClickListener itemListener;

    private List<PictureTask> pictureTasks = new ArrayList<>();

    public RecipientListAdapter(List<User> recipients) {
        this.recipients = recipients != null ? recipients : new ArrayList<>();
    }

    public void addRecipient(User recipient) {
        if (recipients.add(recipient)) {
            notifyItemInserted(recipients.size() - 1);
        }
    }

    public void deleteRecipient(User recipient) {
        int index = recipients.indexOf(recipient);
        if (index != -1) {
            recipients.remove(recipient);
            notifyItemRemoved(index);
        }
    }

    public void setItemOnClickListener(View.OnClickListener itemListener) {
        this.itemListener = itemListener;
    }

    public List<User> getRecipients() {
        return recipients;
    }

    @Override
    public RecipientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView view = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_recipient, parent, false);
        if (itemListener != null) {
            view.setOnClickListener(itemListener);
        }
        return new RecipientViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecipientViewHolder holder, int position) {
        User user = recipients.get(position);
        holder.name.setText(user.getName());
        PictureTask pictureTask = new PictureTask(user, holder.picture, R.drawable.unknown_person);
        pictureTask.execute();
        pictureTasks.add(pictureTask);
    }

    @Override
    public int getItemCount() {
        return recipients.size();
    }

    public void cancelPictureDownloading() {
        for (PictureTask task : pictureTasks) {
            task.cancel(true);
        }
        pictureTasks.clear();
    }

    public class RecipientViewHolder extends ViewHolder  {

        ImageView picture;
        TextView name;

        public RecipientViewHolder(View itemView) {
            super(itemView);
            picture = itemView.findViewById(R.id.recipient_card_vertical_picture);
            name = itemView.findViewById(R.id.recipient_card_vertical_name);
        }
    }
}
