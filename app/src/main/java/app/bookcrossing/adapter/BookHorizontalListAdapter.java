package app.bookcrossing.adapter;


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.bookcrossing.R;
import app.bookcrossing.async.PictureTask;
import app.bookcrossing.client.net.dto.Book;

public class BookHorizontalListAdapter
        extends RecyclerView.Adapter<BookHorizontalListAdapter.BookViewHolder> {

    private List<Book> books;
    private View.OnClickListener listener;

    private List<PictureTask> pictureTasks = new ArrayList<>();

    public BookHorizontalListAdapter(List<Book> books) {
        this.books = books;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setOnBookClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public BookHorizontalListAdapter.BookViewHolder onCreateViewHolder(ViewGroup parent,
                                                                       int viewType) {
        CardView view = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_book_horizontal, parent, false);
        if (listener != null) {
            view.setOnClickListener(listener);
        }
        return new BookHorizontalListAdapter.BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BookHorizontalListAdapter.BookViewHolder holder,
                                 int position) {
        Book book = books.get(position);

        holder.picture.setImageResource(R.drawable.book_flat_circle);
        holder.title.setText(book.getTitle());

        PictureTask pictureTask = new PictureTask(book, holder.picture, R.drawable.book_flat_circle);
        pictureTask.execute();
        pictureTasks.add(pictureTask);
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public void cancelPictureDownloading() {
        for (PictureTask task : pictureTasks) {
            task.cancel(true);
        }
        pictureTasks.clear();
    }

    protected static class BookViewHolder extends RecyclerView.ViewHolder {

        ImageView picture;
        TextView  title;

        public BookViewHolder(View itemView) {
            super(itemView);
            picture = itemView.findViewById(R.id.picture);
            title = itemView.findViewById(R.id.title);
        }
    }
}
