package app.bookcrossing.adapter;


import android.graphics.Bitmap;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.bookcrossing.R;
import app.bookcrossing.async.PictureTask;
import app.bookcrossing.client.net.dto.Book;

import static android.support.v7.widget.RecyclerView.ViewHolder;

public class BookCardListAdapter
        extends RecyclerView.Adapter<BookCardListAdapter.BookViewHolder> {

    private final int layoutId;
    private List<Book> books;
    private List<Book> copy;
    private View.OnClickListener itemListener;

    private SparseArray<Object> pictures = new SparseArray<>();
    private Object pictureStub = new Object();

    private List<PictureTask> pictureTasks = new ArrayList<>();

    public BookCardListAdapter(List<Book> books, @LayoutRes int layoutId) {
        this.books = books != null ? new ArrayList<>(books) : new ArrayList<>();
        this.copy = new ArrayList<>(this.books);
        this.layoutId = layoutId;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setItemOnClickListener(View.OnClickListener itemListener) {
        this.itemListener = itemListener;
    }

    @Override
    public BookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        if (itemListener != null) {
            view.setOnClickListener(itemListener);
        }
        return new BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BookViewHolder holder, int position) {
        Book book = books.get(position);
        holder.title.setText(book.getTitle());
        holder.author.setText(book.getAuthor());

        Object picture = pictures.get(position);
        if (picture != null) {
            if (picture instanceof Bitmap) {
                holder.picture.setImageBitmap((Bitmap) picture);
            } else {
                holder.picture.setImageResource(R.drawable.book_flat_circle);
            }
        } else {
            PictureTask pictureTask = new PictureTask(book, holder.picture, R.drawable.book_flat_circle)
                    .onSuccess(response -> pictures.put(position, response.getPicture().getBitmap()))
                    .onFailed(() -> pictures.put(position, pictureStub));
            pictureTask.execute();
            pictureTasks.add(pictureTask);
        }
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public void cancelPictureDownloading() {
        for (PictureTask task : pictureTasks) {
            task.cancel(true);
        }
        pictureTasks.clear();
    }

    protected static class BookViewHolder extends ViewHolder {

        ImageView picture;
        TextView title;
        TextView author;

        public BookViewHolder(View itemView) {
            super(itemView);
            picture = itemView.findViewById(R.id.book_card_picture);
            title = itemView.findViewById(R.id.book_card_title);
            author = itemView.findViewById(R.id.book_card_author);
        }
    }

    public void filter(String query) {
        books.clear();
        if (query.isEmpty()) {
            books.addAll(copy);
        } else {
            query = query.toLowerCase();
            for (Book book : copy) {
                String id = String.valueOf(book.getId());
                String title = book.getTitle().toLowerCase();
                String[] authors = book.getAuthor().split(" *, *");

                if (id.contains(query) || title.contains(query)) {
                    books.add(book);
                } else {
                    for (String author : authors) {
                        if (author.toLowerCase().contains(query)) {
                            books.add(book);
                        }
                    }
                }
            }
        }
        notifyDataSetChanged();
    }
}
