package app.bookcrossing.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.bookcrossing.R;

import static android.support.v7.widget.RecyclerView.ViewHolder;


public class AuthorCardListAdapter
        extends RecyclerView.Adapter<AuthorCardListAdapter.AuthorCardViewHolder> {

    private List<String> authors;
    private ItemClickListener editListener;
    private ItemClickListener deleteListener;

    public AuthorCardListAdapter(List<String> authors) {
        this.authors = authors != null ? authors : new ArrayList<>();
    }

    public List<String> getAuthors() {
        return authors;
    }

    public String getAuthorsFlatten() {
        StringBuilder builder = new StringBuilder();
        if (!authors.isEmpty()) {
            builder.append(authors.get(0));
            for (int i = 1; i < authors.size(); i++) {
                builder.append(", ").append(authors.get(i).trim());
            }
        }
        return builder.toString();
    }

    public String getAuthor(int position) {
        return authors.get(position);
    }

    public void addAuthor(String author) {
        authors.add(author);
        notifyItemInserted(authors.size() - 1);
    }

    public void deleteAuthor(String author) {
        int index = authors.indexOf(author);
        if (index != -1) {
            authors.remove(author);
            notifyItemRemoved(index);
        }
    }

    public void deleteAuthor(int index) {
        authors.remove(index);
        notifyItemRemoved(index);
    }

    public void setEditListener(ItemClickListener editListener) {
        this.editListener = editListener;
    }

    public void setDeleteListener(ItemClickListener deleteListener) {
        this.deleteListener = deleteListener;
    }

    @Override
    public AuthorCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_author_small, parent, false);
        return new AuthorCardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AuthorCardViewHolder holder, int position) {
        holder.author.setText(authors.get(position));
        holder.edit.setOnClickListener(view -> editListener.onClick(position));
        holder.delete.setOnClickListener(view -> deleteListener.onClick(position));
    }

    @Override
    public int getItemCount() {
        return authors.size();
    }

    protected static class AuthorCardViewHolder extends ViewHolder {

        TextView  author;
        ImageView delete;
        ImageView edit;

        public AuthorCardViewHolder(View itemView) {
            super(itemView);
            author = itemView.findViewById(R.id.author_card_vertical_name);
            delete = itemView.findViewById(R.id.author_card_delete);
            edit = itemView.findViewById(R.id.author_card_edit);
        }
    }

    public interface ItemClickListener {
        void onClick(int position);
    }
}
