package app.bookcrossing.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.bookcrossing.R;
import app.bookcrossing.client.net.dto.Transfer;
import app.bookcrossing.fragment.TransferListFragment.TransferView;
import app.bookcrossing.util.InstantFormatter;

import static android.support.v7.widget.RecyclerView.ViewHolder;
import static app.bookcrossing.fragment.TransferListFragment.TransferView.INCOMING;

public class TransferCardListAdapter
        extends RecyclerView.Adapter<TransferCardListAdapter.TransferViewHolder> {

    private List<Transfer> transfers;
    private View.OnClickListener itemListener;
    private final TransferView view;

    public TransferCardListAdapter(List<Transfer> transfers, TransferView view) {
        this.transfers = transfers != null ? transfers : new ArrayList<>();
        this.view = view;
    }

    public List<Transfer> getTransfers() {
        return transfers;
    }

    public void setItemOnClickListener(View.OnClickListener itemListener) {
        this.itemListener = itemListener;
    }

    public void deleteTransfer(Transfer transfer) {
        int index = -1;
        for (int i = 0; i < transfers.size(); i++) {
            if (transfers.get(i).getId() == transfer.getId()) {
                index = i;
                break;
            }
        }
        if (index != -1) {
            transfers.remove(index);
            notifyItemRemoved(index);
        }
    }

    public void deleteTransfer(long id) {
        int index = -1;
        for (int i = 0; i < transfers.size(); i++) {
            if (transfers.get(i).getId() == id) {
                index = i;
                break;
            }
        }
        if (index != -1) {
            transfers.remove(index);
            notifyItemRemoved(index);
        }
    }

    @Override
    public TransferViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = (view == INCOMING) ? R.layout.card_transfer_incoming : R.layout.card_transfer_outbound;
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        if (itemListener != null) {
            view.setOnClickListener(itemListener);
        }
        return new TransferViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TransferViewHolder holder, int position) {
        Transfer transfer = transfers.get(position);
        holder.user.setText((view == INCOMING ? transfer.getSender() : transfer.getRecipient()).getName());
        holder.book.setText(transfer.getBook().getTitle());
        holder.date.setText(InstantFormatter.getDate(transfer.getSent()));
    }

    @Override
    public int getItemCount() {
        return transfers.size();
    }

    public static class TransferViewHolder extends ViewHolder {

        TextView user;
        TextView book;
        TextView date;

        public TransferViewHolder(View view) {
            super(view);
            user = view.findViewById(R.id.transfer_user);
            book = view.findViewById(R.id.transfer_book_title);
            date = view.findViewById(R.id.transfer_sent_date);
        }
    }
}
