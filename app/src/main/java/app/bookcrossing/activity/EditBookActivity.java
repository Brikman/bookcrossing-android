package app.bookcrossing.activity;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import app.bookcrossing.R;
import app.bookcrossing.async.AsyncClientTask;
import app.bookcrossing.async.GenericAsyncTask;
import app.bookcrossing.async.PictureTask;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.net.dto.Book;
import app.bookcrossing.client.net.dto.Picture;
import app.bookcrossing.client.net.packet.response.StatusResponse;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.dialog.AuthorDialog;
import app.bookcrossing.adapter.AuthorCardListAdapter;
import app.bookcrossing.dialog.ProgressDialog;
import app.bookcrossing.logging.TagHelper;
import app.bookcrossing.util.BitmapUtils;
import app.bookcrossing.util.IntentUtils;
import app.bookcrossing.util.RequestCode;
import app.bookcrossing.util.ResultCode;

public class EditBookActivity extends AppCompatActivity {

    private static final String TAG = TagHelper.getTag();

    private Book book;
    private int  mode;

    private ImageView    bookPicture;
    private EditText     bookTitle;
    private EditText     bookDescription;
    private TextView     charsRemains;
    private ImageView    addAuthor;
    private RecyclerView recyclerView;
    private Button       saveButton;
    private ImageView    deletePicture;

    private AuthorCardListAdapter adapter;

    private Uri bookImageUri;
    private Bitmap bookImageBitmap;

    private ProgressDialog editProgressDialog;
    private PictureTask bookPictureTask;
    private AsyncClientTask<StatusResponse> editBookTask;

    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_edit_book);
        setTitle(getString(R.string.new_book));

        bookPicture = findViewById(R.id.edit_book_picture);
        bookTitle = findViewById(R.id.edit_book_title);
        bookDescription = findViewById(R.id.edit_book_description);
        addAuthor = findViewById(R.id.edit_book_add_author);
        saveButton = findViewById(R.id.edit_book_save);
        saveButton.setVisibility(View.INVISIBLE);
        deletePicture = findViewById(R.id.delete_picture);
        charsRemains = findViewById(R.id.description_chars_remain);
        charsRemains.setText(String.format("Осталось %d/%d", Book.DESCRIPTION_MAX_LENGTH, Book.DESCRIPTION_MAX_LENGTH));
        bookTitle.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            public void afterTextChanged(Editable s) {
                toggleView();
            }
        });
        bookDescription.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void afterTextChanged(Editable s) {
                if (s.length() > Book.DESCRIPTION_MAX_LENGTH) {
                    s.delete(Book.DESCRIPTION_MAX_LENGTH, s.length());
                }
            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                charsRemains.setText(String.format("Осталось %d/%d",
                        Book.DESCRIPTION_MAX_LENGTH - s.length(), Book.DESCRIPTION_MAX_LENGTH));
            }
        });

        book = IntentUtils.getBook(getIntent());
        mode = getIntent().getIntExtra(ExtraKey.EDIT_BOOK_MODE, -1);

        List<String> authors = null;
        if (mode == RequestCode.EDIT_BOOK) {
            bookPictureTask = new PictureTask(book, bookPicture, R.drawable.book_flat_circle);
            bookPictureTask.onSuccess(r -> {
                deletePicture.setVisibility(r != null ? View.VISIBLE : View.INVISIBLE);
                bookImageBitmap = r.getPicture().getBitmap();
            });
            bookPictureTask.execute();
            bookTitle.setText(book.getTitle());
            bookDescription.setText(book.getDescription());
            if (book.getAuthor() != null && !book.getAuthor().trim().isEmpty()) {
                authors = new ArrayList<>(Arrays.asList(book.getAuthor().trim().split(" *, *")));
            }
        }

        recyclerView = findViewById(R.id.edit_book_author_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new AuthorCardListAdapter(authors);
        adapter.setEditListener(this::onAuthorEdit);
        adapter.setDeleteListener(this::onAuthorDelete);
        recyclerView.setAdapter(adapter);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        toggleView();
    }

    private void toggleView() {
        String title = bookTitle.getText().toString().trim();
        List<String> authors = adapter != null ? adapter.getAuthors() : Collections.emptyList();
        if (!title.isEmpty() && !authors.isEmpty()) {
            saveButton.setVisibility(View.VISIBLE);
        } else {
            saveButton.setVisibility(View.INVISIBLE);
        }
    }

    public void onBookSave(View view) {
        if (mode == RequestCode.EDIT_BOOK) {
            editProgressDialog = new ProgressDialog(this::editBook, 1000, "Сохранение...", false);
            editProgressDialog.show(getFragmentManager(), "progress_dialog");
        } else if (mode == RequestCode.CREATE_BOOK) {
            createBook();
        }
    }

    private void editBook() {
        book.setTitle(bookTitle.getText().toString().trim());
        book.setDescription(bookDescription.getText().toString().trim());
        book.setAuthor(((AuthorCardListAdapter) recyclerView.getAdapter()).getAuthorsFlatten());
        if (bookImageBitmap != null)
            book.setPicture(new Picture(BitmapUtils.getBytes(bookImageBitmap)));

        Client client = Client.getInstance();
        editBookTask = new AsyncClientTask<StatusResponse>()
                .forFuture(client.editBook(book))
                .onSuccess(response -> {
                    book.setPicture(null);
                    Intent intent = new Intent();
                    intent.putExtra(ExtraKey.BOOK, book);
                    setResult(ResultCode.SUCCESS_EDIT_BOOK, intent);
                    finish();
                })
                .onRejected(reject -> editProgressDialog.showError("Ошибка при сохранении книги"))
                .onException(reject -> editProgressDialog.showError("Ошибка при сохранении книги"));
        editBookTask.execute();
    }

    private void createBook() {
        Book book = new Book();
        book.setTitle(bookTitle.getText().toString().trim());
        String description = bookDescription.getText().toString().trim();
        if (!description.isEmpty()) {
            book.setDescription(description);
        }
        StringBuilder builder = new StringBuilder();
        List<String> authors = adapter.getAuthors();
        builder.append(authors.get(0));
        for (int i = 1; i < authors.size(); i++) {
            builder.append(", ").append(authors.get(i).trim());
        }
        book.setAuthor(builder.toString());

        Intent data = new Intent();
        data.putExtra(ExtraKey.BOOK, book);
        if (bookImageUri != null) {
            data.putExtra(ExtraKey.IMAGE_URI, bookImageUri.toString());
        }
        setResult(ResultCode.SUCCESS_CREATE_BOOK, data);
        finish();
    }

    public void onAuthorAdd(View view) {
        AuthorDialog dialog = new AuthorDialog(this);
        dialog.show(getFragmentManager(), "author_dialog");
    }

    public boolean addAuthor(String author) {
        if (adapter.getAuthors().contains(author)) {
            return false;
        }
        adapter.addAuthor(author.replaceAll(" +", " ").trim());
        adapter.notifyItemInserted(adapter.getItemCount() - 1);
        if (adapter.getItemCount() == Book.AUTHORS_MAX_COUNT) {
            addAuthor.setVisibility(View.GONE);
        }
        toggleView();
        return true;
    }

    public boolean editAuthor(int position, String author) {
        author = author.replaceAll(" +", " ").trim();
        List<String> authors = adapter.getAuthors();
        for (int i = 0; i < authors.size(); i++) {
            if (i == position) {
                continue;
            }
            if (authors.get(i).equals(author)) {
                return false;
            }
        }
        authors.set(position, author);
        adapter.notifyItemChanged(position);
        return true;
    }

    public List<String> getAuthors() {
        return adapter.getAuthors();
    }

    public void onAuthorEdit(int position) {
        AuthorDialog dialog = new AuthorDialog(this, position);
        dialog.show(getFragmentManager(), "author_dialog");
    }

    public void onAuthorDelete(int position) {
        adapter.deleteAuthor(position);
        if (adapter.getItemCount() < Book.AUTHORS_MAX_COUNT) {
            addAuthor.setVisibility(View.VISIBLE);
        }
        toggleView();
    }

    public void onLoadPicture(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(intent, "Выбрать фото"), RequestCode.PICK_IMAGE);
    }

    public void onDeletePicture(View view) {
        bookPicture.setImageResource(R.drawable.book_flat_circle);
        bookImageUri = null;
        bookImageBitmap = null;
        view.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCode.PICK_IMAGE && resultCode == RESULT_OK) {
            try {
                bookImageUri = data.getData();
                bookImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), bookImageUri);
                new GenericAsyncTask<Bitmap, Bitmap>()
                        .forAction(BitmapUtils::getScaled)
                        .onSuccess(scaledBitmap -> bookPicture.setImageBitmap(scaledBitmap))
                        .execute(bookImageBitmap);
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
                Toast.makeText(this, "Не удалось загрузить изображение", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (editBookTask != null)
            editBookTask.cancel(true);
        if (bookPictureTask != null)
            bookPictureTask.cancel(true);
        super.onBackPressed();
    }
}
