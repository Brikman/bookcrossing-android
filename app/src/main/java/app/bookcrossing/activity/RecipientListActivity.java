package app.bookcrossing.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import app.bookcrossing.R;
import app.bookcrossing.adapter.BookHorizontalListAdapter;
import app.bookcrossing.adapter.RecipientListAdapter;
import app.bookcrossing.async.AsyncClientTask;
import app.bookcrossing.client.ConnectionException;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.future.RejectionException;
import app.bookcrossing.client.net.dto.User;
import app.bookcrossing.client.net.packet.response.RecipientListResponse;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.dialog.RecipientDialog;
import app.bookcrossing.logging.TagHelper;
import app.bookcrossing.util.RequestCode;
import app.bookcrossing.util.ResultCode;

public class RecipientListActivity extends AppCompatActivity
        implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = TagHelper.getTag();

    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private TextView emptyLabel;

    private User recipient;

    private int availableCount;
    private AsyncClientTask<RecipientListResponse> recipientsListTask;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_recipient_list);
        setTitle(R.string.recipient_list_title);

        emptyLabel = findViewById(R.id.empty_recipients_list_label);
        recyclerView = findViewById(R.id.recipient_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        refreshLayout = findViewById(R.id.recipient_refresh_layout);
        refreshLayout.setOnRefreshListener(this);

        onRefresh();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }

    public void requireRecipients(View view) {
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        int pendingCount = adapter != null ? adapter.getItemCount() : 0;
        RecipientDialog dialog = new RecipientDialog(this, pendingCount, availableCount);
        dialog.show(getFragmentManager(), "recipient_dialog");
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(true);
        emptyLabel.setVisibility(View.INVISIBLE);

        Client client = Client.getInstance();
        recipientsListTask = new AsyncClientTask<RecipientListResponse>()
                .forFuture(client.getRecipientsList())
                .onSuccess(this::showRecipients)
                .after(() -> refreshLayout.setRefreshing(false));
        recipientsListTask.execute();
    }

    public void showRecipients(RecipientListResponse response) {
        List<User> recipients = response.getRecipients();
        availableCount = response.getAvailableRecipients();
        RecipientListAdapter adapter;
        if (recipients.size() > 0) {
            adapter = new RecipientListAdapter(recipients);
            adapter.setItemOnClickListener(this::onRecipientClick);
        } else {
            adapter = new RecipientListAdapter(null);
            emptyLabel.setVisibility(View.VISIBLE);
        }
        recyclerView.setAdapter(adapter);
    }

    public void onError(Exception e) {
        Log.e(TAG, e.getMessage(), e);
        runOnUiThread(() -> {
            String message;
            if (e instanceof ConnectionException) {
                message = "Ошибка соединения с сервером";
            } else if (e instanceof RejectionException) {
                message = "Превышен лимит новых получателей";
            } else {
                message = "Внутренняя ошибка";
            }
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        });
    }

    public void onRecipientClick(View view) {
        List<User> collection = ((RecipientListAdapter) recyclerView.getAdapter()).getRecipients();
        recipient = collection.get(recyclerView.getChildLayoutPosition(view));
        Intent intent = new Intent(this, SendBookActivity.class);
        intent.putExtra(ExtraKey.RECIPIENT, recipient);
        startActivityForResult(intent, RequestCode.SEND_BOOK);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCode.SEND_BOOK && resultCode == ResultCode.SUCCESS_SEND_BOOK) {
            ((RecipientListAdapter) recyclerView.getAdapter()).deleteRecipient(recipient);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        recipientsListTask.cancel(true);
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter != null) {
            ((RecipientListAdapter) adapter).cancelPictureDownloading();
        }
        super.onBackPressed();
    }
}
