package app.bookcrossing.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import app.bookcrossing.R;
import app.bookcrossing.async.AsyncClientTask;
import app.bookcrossing.async.PictureTask;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.net.dto.Transfer;
import app.bookcrossing.client.net.packet.response.StatusResponse;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.dialog.ProgressDialog;
import app.bookcrossing.util.InstantFormatter;
import app.bookcrossing.util.RequestCode;

import static app.bookcrossing.fragment.TransferListFragment.TransferView;

public class OutboundTransferActivity extends AppCompatActivity {

    private Transfer transfer;
    private TransferView transferView;

    private ImageView bookPicture;
    private TextView bookTitle;

    private ImageView recipientPicture;
    private TextView recipientName;

    private TextView sentDate;

    private EditText tracknumber;
    private EditText message;
    private Button button;

    private ProgressDialog dialog;

    private PictureTask bookPictureTask;
    private PictureTask recipientPictureTask;
    private AsyncClientTask<StatusResponse> editTransferTask;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(getString(R.string.transfer_title));
        setContentView(R.layout.activity_outbound_transfer);

        transfer = getIntent().getParcelableExtra(ExtraKey.TRANSFER);
        transferView = (TransferView) getIntent().getSerializableExtra(ExtraKey.TRANSFER_VIEW);

        bookPicture = findViewById(R.id.transfer_book_picture);
        bookTitle = findViewById(R.id.transfer_book_title);
        bookPictureTask = new PictureTask(transfer.getBook(), bookPicture, R.drawable.book_flat_circle);
        bookPictureTask.execute();
        bookTitle.setText(transfer.getBook().getTitle());
        findViewById(R.id.transfer_book_container).setOnClickListener(this::onBookClick);

        recipientPicture = findViewById(R.id.transfer_sender_picture);
        recipientName = findViewById(R.id.transfer_sender_name);
        recipientPictureTask = new PictureTask(transfer.getRecipient(), recipientPicture, R.drawable.person_flat_circle);
        recipientPictureTask.execute();
        recipientName.setText(transfer.getRecipient().getName());
        findViewById(R.id.transfer_sender_container).setOnClickListener(this::onRecipientClick);

        sentDate = findViewById(R.id.transfer_sent_date);
        sentDate.setText(InstantFormatter.getDateTime(transfer.getSent()));

        tracknumber = findViewById(R.id.transfer_tracknumber);
        message = findViewById(R.id.transfer_message);

        refreshTransferDetails();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }

    public void refreshTransferDetails() {
        if (transfer.getTrackNumber() != null && !transfer.getTrackNumber().trim().isEmpty()) {
            tracknumber.setText(transfer.getTrackNumber());
        }
        if (transfer.getMessage() != null && !transfer.getMessage().trim().isEmpty()) {
            message.setText(transfer.getMessage());
        }
    }

    public void onBookClick(View view) {
        Intent intent = new Intent(this, BookActivity.class);
        intent.putExtra(ExtraKey.BOOK, transfer.getBook());
        startActivityForResult(intent, RequestCode.TRANSFER_CONFIRM);
    }

    public void onRecipientClick(View view) {
        Intent intent = new Intent(this, UserActivity.class);
        intent.putExtra(ExtraKey.USER, transfer.getRecipient());
        startActivity(intent);
    }

    public void onSaveTransfer(View view) {
        dialog = new ProgressDialog(this::saveTransfer, 500, "Сохранение...", false);
        dialog.setOnAcceptListener(v -> dialog.dismiss());
        dialog.setOnDeclineListener(v -> dialog.dismiss());
        dialog.show(getFragmentManager(), "save_dialog");
    }

    public void saveTransfer() {
        String tracknumber = this.tracknumber.getText().toString().trim();
        String message = this.message.getText().toString().trim();

        Transfer dto = new Transfer();
        dto.setId(transfer.getId());
        dto.setTrackNumber(tracknumber.isEmpty() ? transfer.getTrackNumber() : tracknumber);
        dto.setMessage(message.isEmpty() ? transfer.getMessage() : message);

        Client client = Client.getInstance();
        editTransferTask = new AsyncClientTask<StatusResponse>()
                .forFuture(client.editTransfer(dto))
                .onSuccess(response -> {
                    transfer.setTrackNumber(dto.getTrackNumber());
                    transfer.setMessage(dto.getMessage());
                    refreshTransferDetails();
                    dialog.showSuccess("Изменения сохранены");
                })
                .onRejected(response -> dialog.showError("Ошибка редактирования"))
                .onException(response -> dialog.showError("Ошибка редактирования"));
        editTransferTask.execute();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        bookPictureTask.cancel(true);
        recipientPictureTask.cancel(true);
        super.onBackPressed();
    }
}
