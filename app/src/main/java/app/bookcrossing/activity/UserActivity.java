package app.bookcrossing.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import app.bookcrossing.R;
import app.bookcrossing.adapter.BookHorizontalListAdapter;
import app.bookcrossing.async.AsyncClientTask;
import app.bookcrossing.async.PictureTask;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.net.dto.Book;
import app.bookcrossing.client.net.dto.User;
import app.bookcrossing.client.net.packet.response.EntityListResponse;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.util.IntentUtils;

public class UserActivity extends AppCompatActivity {

    private User user;

    private ImageView userPicture;
    private TextView userName;
    private TextView userEmail;

    private View phoneContainer;
    private TextView userPhone;

    private View addressContainer;
    private TextView userAddress;

    private View aboutContainer;
    private TextView userAbout;

    private TextView collectionTitle;
    private RecyclerView recyclerView;

    private PictureTask pictureTask;
    private AsyncClientTask<EntityListResponse<Book>> booksTask;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        user = IntentUtils.getUser(getIntent());

        userPicture = findViewById(R.id.user_picture);
        userName = findViewById(R.id.user_name);
        userEmail = findViewById(R.id.user_email);

        phoneContainer = findViewById(R.id.user_phone_container);
        userPhone = findViewById(R.id.user_phone);

        addressContainer = findViewById(R.id.user_address_container);
        userAddress = findViewById(R.id.user_address);

        aboutContainer = findViewById(R.id.user_about_container);
        userAbout = findViewById(R.id.user_about);

        collectionTitle = findViewById(R.id.collection_title);
        collectionTitle.setVisibility(View.GONE);
        recyclerView = findViewById(R.id.book_list_horizontal);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        Client client = Client.getInstance();
        booksTask = new AsyncClientTask<EntityListResponse<Book>>()
                .forFuture(client.getBookListByUser(user.getId()))
                .onSuccess(this::showBooks);
        booksTask.execute();

        userName.setText(user.getName());
        userEmail.setText(user.getEmail());

        if (user.getPhone() != null) {
            userPhone.setText(user.getPhone());
        } else {
            phoneContainer.setVisibility(View.GONE);
        }

        if (user.getAddress() != null) {
            userAddress.setText(user.getAddress().toString());
        } else {
            addressContainer.setVisibility(View.GONE);
        }

        if (user.getAbout() != null) {
            userAbout.setText(user.getAbout());
        } else {
            aboutContainer.setVisibility(View.GONE);
        }

        pictureTask = new PictureTask(user, userPicture, R.drawable.person_flat_circle);
        pictureTask.execute();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setElevation(0);
    }

    public void showBooks(EntityListResponse<Book> response) {
        List<Book> books = response.getEntities();
        BookHorizontalListAdapter adapter = new BookHorizontalListAdapter(books);
        adapter.setOnBookClickListener(this::onBookClick);
        recyclerView.setAdapter(adapter);
        if (books.size() > 0) {
            collectionTitle.setVisibility(View.VISIBLE);
        }
    }

    public void onBookClick(View view) {
        List<Book> books = ((BookHorizontalListAdapter) recyclerView.getAdapter()).getBooks();
        Book book = books.get(recyclerView.getChildLayoutPosition(view));

        Intent intent = new Intent(this, BookActivity.class);
        intent.putExtra(ExtraKey.BOOK, book);
        startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        pictureTask.cancel(true);
        booksTask.cancel(true);
        super.onBackPressed();
    }
}
