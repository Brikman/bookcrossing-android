package app.bookcrossing.activity;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import app.bookcrossing.R;
import app.bookcrossing.async.AsyncClientTask;
import app.bookcrossing.async.GenericAsyncTask;
import app.bookcrossing.async.PictureTask;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.net.dto.Address;
import app.bookcrossing.client.net.dto.Picture;
import app.bookcrossing.client.net.dto.User;
import app.bookcrossing.client.net.packet.request.EditAccountRequest;
import app.bookcrossing.client.net.packet.request.EditAccountRequestBuilder;
import app.bookcrossing.client.net.packet.response.StatusResponse;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.dialog.ProgressDialog;
import app.bookcrossing.logging.TagHelper;
import app.bookcrossing.util.BitmapUtils;
import app.bookcrossing.util.IntentUtils;
import app.bookcrossing.util.RequestCode;
import app.bookcrossing.util.ResultCode;

public class EditProfileActivity extends AppCompatActivity {

    private static final String TAG = TagHelper.getTag();

    private User user;

    private ImageView picture;
    private ImageView deletePicture;
    private EditText  name;
    private EditText  phone;
    private TextView  addressTitle;
    private ImageView addressAlert;
    private TextView  address;
    private TextView  charsRemains;
    private EditText  about;

    private ProgressDialog progressDialog;

    private boolean pictureChanged = false;
    private boolean addressChanged = false;

    private PictureTask pictureTask;

    private AsyncClientTask<StatusResponse> editTask;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_edit_profile);
        setTitle(getString(R.string.profile_editing_title));

        user = IntentUtils.getUser(getIntent());

        picture = findViewById(R.id.profile_picture);
        deletePicture = findViewById(R.id.profile_delete_picture);
        deletePicture.setVisibility(View.GONE);

        name = findViewById(R.id.profile_name);
        phone = findViewById(R.id.profile_phone);
        addressTitle = findViewById(R.id.address_title);
        addressAlert = findViewById(R.id.profile_address_alert);
        address = findViewById(R.id.profile_address);
        charsRemains = findViewById(R.id.profile_about_chars_remains);
        about = findViewById(R.id.profile_about);
        about.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void afterTextChanged(Editable s) {
                if (s.length() > User.ABOUT_MAX_LENGTH) {
                    s.delete(User.ABOUT_MAX_LENGTH, s.length());
                }
            }
            @SuppressLint("DefaultLocale")
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                charsRemains.setText(String.format("Осталось %d/%d",
                        User.ABOUT_MAX_LENGTH - s.length(), User.ABOUT_MAX_LENGTH));
            }
        });

        name.setText(user.getName());
        phone.setText(user.getPhone());
        if (user.getAddress() != null) {
            addressAlert.setVisibility(View.GONE);
            address.setText(user.getAddress().toString());
        } else {
            addressAlert.setVisibility(View.VISIBLE);
            address.setText(R.string.empty_address_alert);
            address.setTextColor(ContextCompat.getColor(this, R.color.red));
        }
        about.setText(user.getAbout());

        pictureTask = new PictureTask(user, picture, R.drawable.unknown_person)
                .onSuccess(r -> deletePicture.setVisibility(r != null ? View.VISIBLE : View.INVISIBLE));
        pictureTask.execute();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }

    public void onSave(View view) {
        progressDialog = new ProgressDialog(this::editAccount, 1000, "Сохранение...", true);
        progressDialog.show(getFragmentManager(), "edit_profile_dialog");
    }

    public void editAccount() {
        String name = trimToNull(this.name.getText().toString());
        String phone = trimToNull(this.phone.getText().toString());
        String address = trimToNull(this.address.getText().toString());
        String about = trimToNull(this.about.getText().toString());

        if (name.isEmpty()) {
            this.name.setError(getString(R.string.type_name_error));
            return;
        }

        EditAccountRequestBuilder builder = new EditAccountRequestBuilder();
        if (!user.getName().equals(name)) {
            builder.setName(name);
            user.setName(name);
        }
        if (((user.getPhone() == null) && (phone != null))
                || ((user.getPhone() != null) && !user.getPhone().equals(phone))) {
            builder.setPhone(phone);
            user.setPhone(phone);
        }
        if (((user.getAbout() == null) && (about != null))
                || ((user.getAbout() != null) && !user.getAbout().equals(about))) {
            builder.setAbout(about);
            user.setAbout(about);
        }
        if (pictureChanged) {
            builder.setPicture(user.getPicture());
        }
        if (addressChanged) {
            builder.setAddress(user.getAddress());
        }

        if (builder.isChanged()) {
            Client client = Client.getInstance();
            EditAccountRequest request = builder.build();
            editTask = new AsyncClientTask<StatusResponse>()
                    .forFuture(client.editAccount(request))
                    .onSuccess(response -> {
                        if (response.isSuccess()) {
                            progressDialog.dismiss();
                            Intent intent = new Intent();
                            intent.putExtra(ExtraKey.USER, user);
                            setResult(ResultCode.SUCCESS_EDIT_PROFILE, intent);
                            finish();
                        } else {
                            progressDialog.showError("Не удалось сохранить изменения");
                        }
                    })
                    .onRejected(reject -> Toast.makeText(this, "Не удалось сохранить изменения",
                            Toast.LENGTH_LONG).show())
                    .onException(ex -> Toast.makeText(this, "Не удалось сохранить изменения",
                            Toast.LENGTH_LONG).show());
            editTask.execute();
        } else {
            finish();
        }
    }

    public void onChangeAddress(View view) {
        Intent intent = new Intent(this, AddressEditActivity.class);
        intent.putExtra(ExtraKey.ADDRESS, user.getAddress());
        startActivityForResult(intent, RequestCode.EDIT_ADDRESS);
    }

    public void onLoadPicture(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(intent, "Выбрать фото"), RequestCode.PICK_IMAGE);
    }

    public void onDeletePicture(View view) {
        picture.setImageResource(R.drawable.unknown_person);
        user.setPicture(null);
        pictureChanged = true;
        view.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCode.PICK_IMAGE && resultCode == RESULT_OK && data != null) {
            try {
                Uri uri = data.getData();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                new GenericAsyncTask<Bitmap, Bitmap>()
                        .forAction(BitmapUtils::getScaled)
                        .onSuccess(scaledBitmap -> {
                            pictureTask.cancel(true);
                            picture.setImageBitmap(scaledBitmap);
                            user.setPicture(new Picture(BitmapUtils.getBytes(scaledBitmap)));
                            pictureChanged = true;
                        })
                        .execute(bitmap);
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
                Toast.makeText(this, "Не удалось загрузить изображение", Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == RequestCode.EDIT_ADDRESS && resultCode == ResultCode.SUCCESS_EDIT_ADDRESS
                && data != null) {
            Address address = IntentUtils.getAddress(data);
            user.setAddress(address);
            this.address.setText(address.toString());
            this.address.setTextColor(addressTitle.getCurrentTextColor());
            addressAlert.setVisibility(View.GONE);
            addressChanged = true;
        }
    }

    public void onCancel(View view) {
        onBackPressed();
    }

    private String trimToNull(String str) {
        if (str == null || str.trim().isEmpty()) {
            return null;
        }
        return str.trim();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        pictureTask.cancel(true);
        if (editTask != null)
            editTask.cancel(true);
        super.onBackPressed();
    }
}
