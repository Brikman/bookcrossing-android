package app.bookcrossing.activity;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Locale;

import app.bookcrossing.R;
import app.bookcrossing.async.AsyncClientTask;
import app.bookcrossing.async.GenericAsyncTask;
import app.bookcrossing.async.PictureTask;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.future.ResponseFuture;
import app.bookcrossing.client.net.dto.Book;
import app.bookcrossing.client.net.dto.Picture;
import app.bookcrossing.client.net.dto.Transfer;
import app.bookcrossing.client.net.dto.User;
import app.bookcrossing.client.net.packet.response.StatusResponse;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.dialog.ProgressDialog;
import app.bookcrossing.fragment.RecipientBookFragment;
import app.bookcrossing.logging.TagHelper;
import app.bookcrossing.util.BitmapUtils;
import app.bookcrossing.util.ResultCode;
import de.hdodenhof.circleimageview.CircleImageView;

import static app.bookcrossing.util.RequestCode.*;
import static app.bookcrossing.util.ResultCode.*;

public class SendBookActivity extends AppCompatActivity {

    private static final String TAG = TagHelper.getTag();

    private User recipient;
    private Book book;
    private int bookState = -1;

    private CircleImageView recipientPicture;
    private TextView recipientName;
    private TextView recipientEmail;
    private TextView recipientAddress;
    private TextView addBookHint;
    private TextView charsRemains;
    private EditText tracknumber;
    private EditText message;
    private ImageView addBook;
    private View transferDetails;

    private RecipientBookFragment bookFragment;

    private ProgressDialog dialog;
    private AsyncClientTask<StatusResponse> sendBookTask;

    private PictureTask pictureTask;

    private Uri bookImageUri;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_send_book);
        setTitle(getString(R.string.book_sending));

        recipient = getIntent().getParcelableExtra(ExtraKey.RECIPIENT);

        recipientPicture = findViewById(R.id.recipient_picture);
        recipientName = findViewById(R.id.recipient_name);
        recipientEmail = findViewById(R.id.recipient_email);
        recipientAddress = findViewById(R.id.recipient_address);
        transferDetails = findViewById(R.id.recipient_transfer_details);
        addBookHint = findViewById(R.id.recipient_add_book_hint);
        addBook = findViewById(R.id.recipient_add_book);
        charsRemains = findViewById(R.id.recipient_message_chars_remain);
        charsRemains.setText(String.format(Locale.US,
                "Осталось %d/%d", Transfer.MESSAGE_MAX_LENGTH, Transfer.MESSAGE_MAX_LENGTH));
        tracknumber = findViewById(R.id.recipient_tracknumber);
        message = findViewById(R.id.recipient_message);
        message.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void afterTextChanged(Editable s) {
                if (s.length() > Transfer.MESSAGE_MAX_LENGTH) {
                    s.delete(Transfer.MESSAGE_MAX_LENGTH, s.length());
                }
            }
            @SuppressLint("DefaultLocale")
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                charsRemains.setText(String.format("Осталось %d/%d",
                        Transfer.MESSAGE_MAX_LENGTH - s.length(), Transfer.MESSAGE_MAX_LENGTH));
            }
        });

        setSelectedBook();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setElevation(0);

        recipientName.setText(recipient.getName());
        recipientEmail.setText(recipient.getEmail());
        recipientAddress.setText(recipient.getAddress().toString());
        pictureTask = new PictureTask(recipient, recipientPicture, R.drawable.unknown_person);
        pictureTask.execute();
    }

    public void setSelectedBook() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (book != null) {
            bookFragment = new RecipientBookFragment(book);
            transaction.add(R.id.recipient_book_container, bookFragment);
            transferDetails.setVisibility(View.VISIBLE);
            addBook.setVisibility(View.GONE);
            addBookHint.setText(getString(R.string.book));
        } else {
            if (bookFragment != null) {
                transaction.remove(bookFragment);
            }
            transferDetails.setVisibility(View.GONE);
            addBook.setVisibility(View.VISIBLE);
            addBookHint.setText(getString(R.string.add_book));
        }
        transaction.commitAllowingStateLoss();

        if (bookState == SUCCESS_CREATE_BOOK && bookImageUri != null) {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), bookImageUri);
                new GenericAsyncTask<Bitmap, Bitmap>()
                        .forAction(BitmapUtils::getScaled)
                        .onSuccess(scaledBitmap -> {
                            bookFragment.getBookPicture().setImageBitmap(scaledBitmap);
                            book.setPicture(new Picture(BitmapUtils.getBytes(scaledBitmap)));
                        })
                        .execute(bitmap);
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
                Toast.makeText(this, "Не удалось загрузить изображение", Toast.LENGTH_SHORT).show();
            }
        } else if (bookState == SUCCESS_SELECT_BOOK) {
            new PictureTask(book, bookFragment.getBookPicture(), R.drawable.book_flat_circle).execute();
        }
    }

    public void onBookAdd(View view) {
        Intent intent = new Intent(this, SelectBookActivity.class);
        startActivityForResult(intent, SELECT_BOOK);
    }

    public void onBookClick(View view) {
        Intent intent = new Intent(this, BookActivity.class);
        intent.putExtra(ExtraKey.BOOK, book);
        startActivity(intent);
    }

    public void onBookDelete(View view) {
        book = null;
        setSelectedBook();
    }

    public void onBookSend(View view) {
        dialog = new ProgressDialog(this::sendBook, 1500, "Выполняется отправка...", true);
        dialog.setOnAcceptListener(v -> {
            dialog.dismiss();
            setResult(ResultCode.SUCCESS_SEND_BOOK);
            finish();
        });
        dialog.setOnDeclineListener(v -> dialog.dismiss());
        dialog.show(getFragmentManager(), "send_book_dialog");
    }

    public void onRecipientClick(View view) {
        Intent intent = new Intent(this, UserActivity.class);
        intent.putExtra(ExtraKey.USER, recipient);
        startActivity(intent);
    }

    public void sendBook() {
        String tracknumber = this.tracknumber.getText().toString().trim();
        if (tracknumber.isEmpty())
            tracknumber = null;
        String message = this.message.getText().toString().trim();
        if (message.isEmpty())
            message = null;
        Client client = Client.getInstance();

        ResponseFuture<StatusResponse> future;
        if (bookState == SUCCESS_CREATE_BOOK) {
            future = client.sendBook(book, recipient.getId(), message, tracknumber);
        } else if (bookState == SUCCESS_SELECT_BOOK) {
            future = client.sendBook(book.getId(), recipient.getId(), message, tracknumber);
        } else {
            return;
        }
        sendBookTask = new AsyncClientTask<StatusResponse>()
                .forFuture(future)
                .onSuccess(status -> dialog.showSuccess("Книга успешно отправлена"))
                .onRejected(reject -> dialog.showError("Произошла ошибка при отправке книги"))
                .onException(exception -> dialog.showError("Произошла ошибка при отправке книги"));
        sendBookTask.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_BOOK && data != null
                && (resultCode == SUCCESS_SELECT_BOOK || resultCode == SUCCESS_CREATE_BOOK)) {
            bookState = resultCode;
            book = data.getParcelableExtra(ExtraKey.BOOK);
            String uriString = data.getStringExtra(ExtraKey.IMAGE_URI);
            if (uriString != null) {
                bookImageUri = Uri.parse(uriString);
            }
            setSelectedBook();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (sendBookTask != null)
            sendBookTask.cancel(true);
        pictureTask.cancel(true);
        super.onBackPressed();
    }
}
