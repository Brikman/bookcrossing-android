package app.bookcrossing.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import app.bookcrossing.R;
import app.bookcrossing.adapter.ViewPagerAdapter;
import app.bookcrossing.async.PictureTask;
import app.bookcrossing.client.net.dto.Book;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.fragment.BookCrossingFragment;
import app.bookcrossing.fragment.BookInfoFragment;
import app.bookcrossing.fragment.ReviewListFragment;
import app.bookcrossing.util.IntentUtils;
import app.bookcrossing.util.RequestCode;
import app.bookcrossing.util.ResultCode;

public class BookActivity extends AppCompatActivity {

    private ImageView bookPicture;
    private TextView  bookTitle;
    private TextView  bookAuthor;
    private TextView  bookRating;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    private FragmentManager  fragmentManager;
    private ViewPagerAdapter pagerAdapter;

    private BookInfoFragment     infoFragment;
    private BookCrossingFragment crossingFragment;
    private ReviewListFragment   reviewFragment;

    private Book book;

    private PictureTask bookPictureTask;
    private boolean editable;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_book);

        book = getIntent().getParcelableExtra(ExtraKey.BOOK);
        editable = getIntent().getBooleanExtra(ExtraKey.BOOK_EDITABLE, false);

        bookPicture = findViewById(R.id.book_picture);
        bookTitle = findViewById(R.id.book_title);
        bookAuthor = findViewById(R.id.book_author);
        bookRating = findViewById(R.id.book_rating);

        viewPager = findViewById(R.id.book_view_pager);
        tabLayout = findViewById(R.id.book_tab_layout);
        fragmentManager = getSupportFragmentManager();

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        bookTitle.setText(book.getTitle());
        bookAuthor.setText(book.getAuthor());
        bookRating.setText(String.format(Locale.US, "%.2f", book.getRating()));
        bookPictureTask = new PictureTask(book, bookPicture, R.drawable.book_flat_circle);
        bookPictureTask.execute();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setElevation(0);
    }

    public void setupViewPager(ViewPager viewPager) {
        infoFragment = new BookInfoFragment(getString(R.string.book_info_page_title), book);
        reviewFragment = new ReviewListFragment(getString(R.string.review_page_title), book);
        crossingFragment = new BookCrossingFragment(getString(R.string.book_crossing_page_title), book, editable);

        pagerAdapter = new ViewPagerAdapter(fragmentManager);
        pagerAdapter.add(infoFragment);
        pagerAdapter.add(reviewFragment);
        pagerAdapter.add(crossingFragment);
        viewPager.setAdapter(pagerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (editable) {
            getMenuInflater().inflate(R.menu.book_activity_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.book_activity_edit) {
            Intent intent = new Intent(this, EditBookActivity.class);
            intent.putExtra(ExtraKey.BOOK, book);
            intent.putExtra(ExtraKey.EDIT_BOOK_MODE, RequestCode.EDIT_BOOK);
            startActivityForResult(intent, RequestCode.EDIT_BOOK);
        } else {
            onBackPressed();
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCode.EDIT_BOOK && resultCode == ResultCode.SUCCESS_EDIT_BOOK
                && data != null) {
            book = IntentUtils.getBook(data);
            bookTitle.setText(book.getTitle());
            bookAuthor.setText(book.getAuthor());
            bookPictureTask.cancel(true);
            bookPictureTask = new PictureTask(book, bookPicture, R.drawable.book_flat_circle);
            bookPictureTask.execute();
            infoFragment.update(book);
        }
    }

    @Override
    public void onBackPressed() {
        bookPictureTask.cancel(true);
        reviewFragment.cancelFutures();
        crossingFragment.cancelFutures();
        super.onBackPressed();
    }
}
