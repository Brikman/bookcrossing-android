package app.bookcrossing.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import app.bookcrossing.R;
import app.bookcrossing.client.net.dto.Address;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.util.IntentUtils;
import app.bookcrossing.util.ResultCode;

public class AddressEditActivity extends AppCompatActivity {

    private Address address;

    private EditText country;
    private EditText city;
    private EditText street;
    private EditText house;
    private EditText apartment;
    private EditText postalCode;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_edit_address);
        setTitle(getString(R.string.address_activity_title));

        address = IntentUtils.getAddress(getIntent());

        country = findViewById(R.id.country);
        city = findViewById(R.id.city);
        street = findViewById(R.id.street);
        house = findViewById(R.id.house);
        apartment = findViewById(R.id.apartment);
        postalCode = findViewById(R.id.postal_code);

        if (address != null) {
            country.setText(trimNullable(address.getCountryName()));
            city.setText(trimNullable(address.getCityName()));
            street.setText(trimNullable(address.getStreet()));
            house.setText(trimNullable(address.getHouse()));
            apartment.setText(trimNullable(address.getApartment()));
            postalCode.setText(trimNullable(address.getPostalCode()));
        }
    }

    public void onSave(View view) {
        Address address = new Address();
        address.setCountryName(trimToNull(country));
        address.setCityName(trimToNull(city));
        address.setStreet(trimToNull(street));
        address.setHouse(trimToNull(house));
        address.setApartment(trimToNull(apartment));
        address.setPostalCode(trimToNull(postalCode));

        if (address.getCountryName() == null) {
            country.setError("Укажите страну");
            return;
        }
        if (address.getCityName() == null) {
            city.setError("Укажите город");
            return;
        }
        if (address.getStreet() == null) {
            street.setError("Укажите улицу");
            return;
        }
        if (address.getHouse() == null) {
            house.setError("Укажите дом");
            return;
        }
        if (address.getPostalCode() == null) {
            postalCode.setError("Укажите почтовый индекс");
            return;
        }

        Intent intent = new Intent();
        intent.putExtra(ExtraKey.ADDRESS, address);
        setResult(ResultCode.SUCCESS_EDIT_ADDRESS, intent);
        finish();
    }

    public void onCancel(View view) {
        finish();
    }

    private String trimNullable(String string) {
        return string == null ? "" : string.trim();
    }

    private String trimToNull(EditText editText) {
        String value = editText.getText().toString().trim();
        return value.isEmpty() ? null : value;
    }
}
