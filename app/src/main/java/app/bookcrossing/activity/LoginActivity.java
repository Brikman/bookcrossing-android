package app.bookcrossing.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.commons.validator.routines.EmailValidator;

import app.bookcrossing.R;
import app.bookcrossing.async.GenericAsyncTask;
import app.bookcrossing.client.ConnectionException;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.future.RejectionException;
import app.bookcrossing.client.net.dto.User;
import app.bookcrossing.client.net.packet.response.EntityResponse;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.logging.TagHelper;
import io.netty.channel.ChannelFuture;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = TagHelper.getTag();

    private EditText loginText;
    private EditText passwordText;
    private Button loginButton;
    private Button registrationButton;
    private ImageView loginButtonArrow;
    private ProgressBar progressBar;

    private EmailValidator emailValidator = EmailValidator.getInstance();

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_login);
        setTitle(R.string.login_title);

        loginText = findViewById(R.id.login_text);
        passwordText = findViewById(R.id.password_text);
        loginButton = findViewById(R.id.login_button);
        registrationButton = findViewById(R.id.login_registration_button);
        progressBar = findViewById(R.id.progress_spinner);
        loginButtonArrow = findViewById(R.id.login_button_arrow);

        loginText.setText("admin@gmail.com");
        passwordText.setText("admin");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Client client = Client.getInstance();
        if (client.isConnected()) {
            new GenericAsyncTask<Void, ChannelFuture>()
                    .forAction(param -> {
                        client.logout();
                        return null;
                    })
                    .onException(this::onError)
                    .execute();
        }
        toggleView(true);
    }

    public void onClickLogin(View view) {
        String login = loginText.getText().toString().trim();
        if (login.isEmpty()) {
            loginText.setError(getString(R.string.empty_login_alert));
            return;
        }
        if (!emailValidator.isValid(login)) {
            loginText.setError(getString(R.string.login_email_format_alert));
            return;
        }
        String password = passwordText.getText().toString().trim();
        if (password.isEmpty()) {
            passwordText.setError(getString(R.string.empty_password_alert));
            return;
        }

        Client client = Client.getInstance();
        new GenericAsyncTask<Void, EntityResponse<User>>()
                .before(() -> toggleView(false))
                .forAction(param -> client.login(login, password).get())
                .onSuccess(this::onSuccess)
                .onException(this::onError)
                .execute();
    }

    private void onSuccess(EntityResponse<User> response) {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra(ExtraKey.USER, response.getEntity());
        startActivity(intent);
    }

    private void onError(Exception e) {
        Log.e(TAG, e.getMessage(), e);
        runOnUiThread(() -> {
            String message;
            if (e instanceof ConnectionException) {
                message = "Ошибка соединения с сервером";
            } else if (e instanceof RejectionException) {
                message = "Неверный логин или пароль";
            } else {
                message = "Внутренняя ошибка";
            }
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            toggleView(true);
        });
    }

    public void onClickRegistrationLink(View view) {
        startActivity(new Intent(this, RegistrationActivity.class));
    }

    private void toggleView(boolean state) {
        Log.i(TAG, "1) TOGGLE VIEW " + state);
        loginText.setEnabled(state);
        passwordText.setEnabled(state);
        loginButton.setEnabled(state);
        registrationButton.setEnabled(state);
        loginButtonArrow.setVisibility(state ? View.VISIBLE : View.INVISIBLE);
        progressBar.setVisibility(state ? View.INVISIBLE : View.VISIBLE);
        Log.i(TAG, "2) TOGGLE VIEW " + state);
    }
}
