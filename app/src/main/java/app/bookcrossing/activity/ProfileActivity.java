package app.bookcrossing.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import app.bookcrossing.R;
import app.bookcrossing.async.PictureTask;
import app.bookcrossing.client.net.dto.User;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.logging.TagHelper;
import app.bookcrossing.util.IntentUtils;
import app.bookcrossing.util.RequestCode;
import app.bookcrossing.util.ResultCode;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = TagHelper.getTag();

    private User user;

    private CircleImageView picture;

    private TextView name;
    private TextView email;
    private TextView phone;
    private TextView address;
    private TextView about;

    private View alertAddressView;

    private PictureTask pictureTask;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_profile);
        setTitle(R.string.profile_title);

        user = getIntent().getParcelableExtra(ExtraKey.USER);

        picture = findViewById(R.id.profile_picture);
        name = findViewById(R.id.profile_name);
        email = findViewById(R.id.profile_email);
        phone = findViewById(R.id.profile_phone);
        address = findViewById(R.id.profile_address);
        about = findViewById(R.id.profile_about);

        alertAddressView = findViewById(R.id.profile_address_alert);

        updateProfile();
    }

    private void updateProfile() {
        TextView textName = findViewById(R.id.profile_phone);
        textName.setText(user.getName() != null ? user.getName() : user.getEmail());

        if (pictureTask != null && pictureTask.getStatus() != AsyncTask.Status.FINISHED) {
            pictureTask.cancel(true);
        }
        pictureTask = new PictureTask(user, picture, R.drawable.unknown_person);
        pictureTask.execute();

        name.setText(user.getName());

        if (user.getEmail() != null) {
            email.setText(user.getEmail());
            findViewById(R.id.profile_email_container).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.profile_email_container).setVisibility(View.GONE);
        }
        if (user.getPhone() != null) {
            phone.setText(user.getPhone());
            findViewById(R.id.profile_phone_container).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.profile_phone_container).setVisibility(View.GONE);
        }
        if (user.getAddress() != null) {
            alertAddressView.setVisibility(View.GONE);
            String addr = user.getAddress().toStringCompact();
            address.setTextColor(email.getCurrentTextColor());
            address.setText(addr);
        } else {
            address.setText(R.string.empty_address_alert);
            address.setTextColor(ContextCompat.getColor(this, R.color.red));
        }
        if (user.getAbout() != null) {
            about.setText(user.getAbout());
            findViewById(R.id.profile_about_container).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.profile_about_container).setVisibility(View.GONE);
        }
    }

    public void openCollection(View view) {
        Intent intent = new Intent(this, LibraryActivity.class);
        intent.putExtra(ExtraKey.USER, user);
        startActivity(intent);
    }

    public void openTransferList(View view) {
        Intent intent = new Intent(this, TransferListActivity.class);
        intent.putExtra(ExtraKey.USER, user);
        startActivity(intent);
    }

    public void openSendBook(View view) {
        if (user.getAddress() != null) {
            Intent intent = new Intent(this, RecipientListActivity.class);
            intent.putExtra(ExtraKey.USER, user);
            startActivity(intent);
        } else {
            Toast.makeText(this, getString(R.string.empty_address_alert), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.book_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(this, EditProfileActivity.class);
        intent.putExtra(ExtraKey.USER, user);
        startActivityForResult(intent, RequestCode.EDIT_PROFILE);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCode.EDIT_PROFILE && resultCode == ResultCode.SUCCESS_EDIT_PROFILE
                && data != null) {
            user = IntentUtils.getUser(data);
            updateProfile();
        }
    }

    @Override
    public void onBackPressed() {
        if (pictureTask != null) {
            pictureTask.cancel(true);
        }
        super.onBackPressed();
    }
}
