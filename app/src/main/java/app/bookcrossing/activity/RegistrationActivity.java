package app.bookcrossing.activity;


import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.commons.validator.routines.EmailValidator;

import app.bookcrossing.R;
import app.bookcrossing.async.AsyncClientTask;
import app.bookcrossing.async.GenericAsyncTask;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.net.dto.User;
import app.bookcrossing.client.net.packet.response.EntityResponse;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.logging.TagHelper;

public class RegistrationActivity extends AppCompatActivity {

    private static final String TAG = TagHelper.getTag();

    private EditText loginText;
    private EditText nameText;
    private EditText passwordText;
    private EditText repeatPasswordText;
    private Button button;
    private ProgressBar progressBar;

    private EmailValidator emailValidator = EmailValidator.getInstance();

    private AsyncClientTask<EntityResponse<User>> registrationTask;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(R.string.registration_title);
        setContentView(R.layout.activity_registration);

        loginText = findViewById(R.id.registration_login_text);
        nameText = findViewById(R.id.registration_name_text);
        passwordText = findViewById(R.id.registration_password_text);
        repeatPasswordText = findViewById(R.id.registration_repeat_password_text);
        button = findViewById(R.id.registration_button);
        progressBar = findViewById(R.id.progress_spinner);
        progressBar.setVisibility(View.INVISIBLE);

        loginText.setOnFocusChangeListener((view, hasFocus) -> {
            if (!hasFocus) {
                String login = loginText.getText().toString().trim();
                String name = nameText.getText().toString().trim();
                if (!login.isEmpty() && !validateLogin()) {
                    loginText.setError(getString(R.string.login_email_format_alert));
                }
            }
        });

        passwordText.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            public void afterTextChanged(Editable s) {
                String password = passwordText.getText().toString().trim();
                if (password.isEmpty()) {
                    repeatPasswordText.setText("");
                    repeatPasswordText.setEnabled(false);
                } else {
                    repeatPasswordText.setEnabled(true);
                }
            }
        });

        repeatPasswordText.setOnFocusChangeListener((view, hasFocus) -> {
            if (!hasFocus && !validatePasswords()) {
                repeatPasswordText.setError(getString(R.string.passwords_not_equals_alert));
            }
        });
    }

    private boolean validateLogin() {
        String login = loginText.getText().toString().trim();
        return emailValidator.isValid(login);
    }

    private boolean validatePasswords() {
        String password = passwordText.getText().toString().trim();
        String repeated = repeatPasswordText.getText().toString().trim();
        return repeated.equals(password);
    }

    public void onClickRegistration(View view) {
        if (!validateLogin()) {
            loginText.setError(getString(R.string.login_email_format_alert));
            return;
        }
        if (!validatePasswords()) {
            passwordText.setError(getString(R.string.passwords_not_equals_alert));
            return;
        }

        String login = loginText.getText().toString().trim();
        String password = passwordText.getText().toString().trim();
        String name = nameText.getText().toString().trim();

        if (name.isEmpty()) {
            nameText.setError("Укажите своё имя");
            return;
        }

        toggleView(false);
        Client client = Client.getInstance();
        registrationTask = new AsyncClientTask<EntityResponse<User>>()
                .forFuture(client.register(login, password, name))
                .onSuccess(this::onSuccess)
                .onException(this::onError);
        registrationTask.execute();
    }

    private void onSuccess(EntityResponse<User> response) {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra(ExtraKey.USER, response.getEntity());
        startActivity(intent);
    }

    private void onError(Exception e) {
        Log.e(TAG, e.getMessage(), e);
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        toggleView(true);
    }

    private void toggleView(boolean state) {
        Resources resources = getResources();
        int enabledColor = resources.getColor(R.color.mainBlue);
        int disabledColor = resources.getColor(R.color.grey);
        loginText.setEnabled(state);
        passwordText.setEnabled(state);
        button.setEnabled(state);
        button.setBackgroundColor(state ? enabledColor : disabledColor);
        progressBar.setVisibility(state ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        if (registrationTask != null)
            registrationTask.cancel(true);
        super.onBackPressed();
    }
}
