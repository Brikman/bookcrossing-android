package app.bookcrossing.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import app.bookcrossing.R;
import app.bookcrossing.adapter.ViewPagerAdapter;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.fragment.TransferListFragment;
import app.bookcrossing.fragment.TransferListFragment.TransferView;
import app.bookcrossing.util.IntentUtils;
import app.bookcrossing.util.RequestCode;
import app.bookcrossing.util.ResultCode;

public class TransferListActivity extends AppCompatActivity {

    private ViewPagerAdapter pagerAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private FragmentManager fragmentManager;
    private TransferListFragment inFragment;
    private TransferListFragment outFragment;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_transfer_list);
        setTitle(R.string.transfer_list_title);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setElevation(0);

        viewPager = findViewById(R.id.transfer_view_pager);
        tabLayout = findViewById(R.id.transfer_tab_layout);
        fragmentManager = getSupportFragmentManager();

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void setupViewPager(ViewPager viewPager) {
        inFragment = new TransferListFragment(getString(R.string.incoming_title), TransferView.INCOMING);
        outFragment = new TransferListFragment(getString(R.string.outbound_title), TransferView.OUTBOUND);

        pagerAdapter = new ViewPagerAdapter(fragmentManager);
        pagerAdapter.add(inFragment);
        pagerAdapter.add(outFragment);
        viewPager.setAdapter(pagerAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        int request = IntentUtils.getRequestCode(data);
        int result  = IntentUtils.getResultCode(data);
        if (request == RequestCode.TRANSFER_CONFIRM && result == ResultCode.SUCCESS_TRANSFER_CONFIRM) {
            long id = data.getLongExtra(ExtraKey.TRANSFER_ID, -1);
            inFragment.getTransferAdapter().deleteTransfer(id);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
