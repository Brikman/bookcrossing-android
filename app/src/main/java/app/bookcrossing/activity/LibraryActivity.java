package app.bookcrossing.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import app.bookcrossing.R;
import app.bookcrossing.adapter.ViewPagerAdapter;
import app.bookcrossing.fragment.BookListFragment;

public class LibraryActivity extends AppCompatActivity {

    private ViewPagerAdapter pagerAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private FragmentManager fragmentManager;

    private BookListFragment collectionFragment;
    private BookListFragment historyFragment;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_library);
        setTitle(getString(R.string.library_title));

        viewPager = findViewById(R.id.library_view_pager);
        tabLayout = findViewById(R.id.library_tab_layout);
        fragmentManager = getSupportFragmentManager();

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setElevation(0);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }

    public void setupViewPager(ViewPager viewPager) {
        collectionFragment = new BookListFragment(getString(R.string.collection_title), BookListFragment.ListView.COLLECTION);
        historyFragment = new BookListFragment(getString(R.string.history_title), BookListFragment.ListView.HISTORY);

        pagerAdapter = new ViewPagerAdapter(fragmentManager);
        pagerAdapter.add(collectionFragment);
        pagerAdapter.add(historyFragment);
        viewPager.setAdapter(pagerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_library_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String query) {
                collectionFragment.getAdapter().filter(query);
                historyFragment.getAdapter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        collectionFragment.cancelFutures();
        historyFragment.cancelFutures();
        super.onBackPressed();
    }
}
