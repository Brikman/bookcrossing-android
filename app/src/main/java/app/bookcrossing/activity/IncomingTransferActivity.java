package app.bookcrossing.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import app.bookcrossing.R;
import app.bookcrossing.async.AsyncClientTask;
import app.bookcrossing.async.PictureTask;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.net.dto.Transfer;
import app.bookcrossing.client.net.packet.response.StatusResponse;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.dialog.ConfirmTransferDialog;
import app.bookcrossing.dialog.MessageDialog;
import app.bookcrossing.util.InstantFormatter;
import app.bookcrossing.util.RequestCode;
import app.bookcrossing.util.ResultCode;

import static app.bookcrossing.fragment.TransferListFragment.TransferView;

public class IncomingTransferActivity extends AppCompatActivity {

    private Transfer transfer;
    private TransferView transferView;

    private ImageView bookPicture;
    private TextView bookTitle;

    private ImageView senderPicture;
    private TextView senderName;

    private TextView sentDate;

    private View tracknumberContainer;
    private TextView tracknumber;

    private View messageContainer;
    private TextView message;
    private TextView showMessage;

    private Button button;

    private ConfirmTransferDialog dialog;

    private PictureTask bookPictureTask;
    private PictureTask senderPictureTask;
    private AsyncClientTask<StatusResponse> confirmTransferTask;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle(getString(R.string.transfer_title));
        setContentView(R.layout.activity_incoming_transfer);

        transfer = getIntent().getParcelableExtra(ExtraKey.TRANSFER);
        transferView = (TransferView) getIntent().getSerializableExtra(ExtraKey.TRANSFER_VIEW);

        bookPicture = findViewById(R.id.transfer_book_picture);
        bookTitle = findViewById(R.id.transfer_book_title);
        bookPictureTask = new PictureTask(transfer.getBook(), bookPicture, R.drawable.book_flat_circle);
        bookPictureTask.execute();
        bookTitle.setText(transfer.getBook().getTitle());
        findViewById(R.id.transfer_book_container).setOnClickListener(this::onBookClick);

        senderPicture = findViewById(R.id.transfer_sender_picture);
        senderName = findViewById(R.id.transfer_sender_name);
        senderPictureTask = new PictureTask(transfer.getSender(), senderPicture, R.drawable.person_flat_circle);
        senderPictureTask.execute();
        senderName.setText(transfer.getSender().getName());
        findViewById(R.id.transfer_sender_container).setOnClickListener(this::onSenderClick);

        sentDate = findViewById(R.id.transfer_sent_date);
        sentDate.setText(InstantFormatter.getDateTime(transfer.getSent()));

        tracknumberContainer = findViewById(R.id.transfer_tracknumber_container);
        tracknumber = findViewById(R.id.transfer_tracknumber);

        messageContainer = findViewById(R.id.transfer_message_container);
        message = findViewById(R.id.transfer_message);
        showMessage = findViewById(R.id.transfer_show_message);

        if (transfer.getTrackNumber() != null && !transfer.getTrackNumber().trim().isEmpty()) {
            tracknumber.setText(transfer.getTrackNumber());
        } else {
            tracknumberContainer.setVisibility(View.GONE);
        }

        if (transfer.getMessage() != null && !transfer.getMessage().trim().isEmpty()) {
            message.setText(transfer.getMessage());
        } else {
            messageContainer.setVisibility(View.GONE);
        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }

    public void onBookClick(View view) {
        Intent intent = new Intent(this, BookActivity.class);
        intent.putExtra(ExtraKey.BOOK, transfer.getBook());
        startActivityForResult(intent, RequestCode.TRANSFER_CONFIRM);
    }

    public void onSenderClick(View view) {
        Intent intent = new Intent(this, UserActivity.class);
        intent.putExtra(ExtraKey.USER, transfer.getSender());
        startActivity(intent);
    }

    public void onShowMessage(View view) {
        String message = this.message.getText().toString();
        new MessageDialog(message).show(getFragmentManager(), "message_dialog");
    }

    public void onConfirmTransfer(View view) {
        dialog = new ConfirmTransferDialog();
        dialog.setOnConfirmListener(this::confirmTransfer);
        dialog.setOnSuccessListener(this::onSuccess);
        dialog.show(getFragmentManager(), "confirm_dialog");
    }

    public void confirmTransfer(View view) {
        Client client = Client.getInstance();
        confirmTransferTask = new AsyncClientTask<StatusResponse>()
                .forFuture(client.confirmTransfer(transfer.getId(), null))
                .onSuccess(response -> dialog.showSuccess())
                .onRejected(reject -> dialog.showError())
                .onException(reject -> dialog.showError());
        confirmTransferTask.execute();
    }

    public void onSuccess(View view) {
        dialog.dismiss();
        Intent intent = new Intent();
        intent.putExtra(ExtraKey.REQUEST_CODE, RequestCode.TRANSFER_CONFIRM);
        intent.putExtra(ExtraKey.RESULT_CODE, ResultCode.SUCCESS_TRANSFER_CONFIRM);
        intent.putExtra(ExtraKey.TRANSFER_ID, transfer.getId());
        setResult(ResultCode.SUCCESS_TRANSFER_CONFIRM, intent);
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        bookPictureTask.cancel(true);
        senderPictureTask.cancel(true);
        if (confirmTransferTask != null)
            confirmTransferTask.cancel(true);
        super.onBackPressed();
    }
}
