package app.bookcrossing.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import app.bookcrossing.R;
import app.bookcrossing.adapter.BookCardListAdapter;
import app.bookcrossing.async.AsyncClientTask;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.net.dto.Book;
import app.bookcrossing.client.net.packet.response.EntityListResponse;
import app.bookcrossing.commons.ExtraKey;
import app.bookcrossing.util.RequestCode;
import app.bookcrossing.util.ResultCode;

public class SelectBookActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private View collectionSelector;

    private AsyncClientTask<EntityListResponse<Book>> collectionTask;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_select_book);
        setTitle(getString(R.string.book_selection));

        collectionSelector = findViewById(R.id.select_from_collection);
        recyclerView = findViewById(R.id.book_selection_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        onRefresh();
    }

    public void onRefresh() {
        collectionSelector.setVisibility(View.GONE);
        Client client = Client.getInstance();
        collectionTask = new AsyncClientTask<EntityListResponse<Book>>()
                .forFuture(client.getBookList())
                .onSuccess(this::showCollection);
        collectionTask.execute();
    }

    private void showCollection(EntityListResponse<Book> response) {
        List<Book> books = response.getEntities();
        if (!books.isEmpty()) {
            BookCardListAdapter adapter = new BookCardListAdapter(books, R.layout.card_book_compact);
            adapter.setItemOnClickListener(this::selectBook);
            recyclerView.setAdapter(adapter);
            collectionSelector.setVisibility(View.VISIBLE);
        }
    }

    public void selectBook(View view) {
        List<Book> books = ((BookCardListAdapter) recyclerView.getAdapter()).getBooks();
        Book book = books.get(recyclerView.getChildLayoutPosition(view));
        Intent data = new Intent();
        data.putExtra(ExtraKey.BOOK, book);
        setResult(ResultCode.SUCCESS_SELECT_BOOK, data);
        finish();
    }

    public void createBook(View view) {
        Intent intent = new Intent(this, EditBookActivity.class);
        intent.putExtra(ExtraKey.EDIT_BOOK_MODE, RequestCode.CREATE_BOOK);
        startActivityForResult(intent, RequestCode.CREATE_BOOK);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCode.CREATE_BOOK && resultCode == ResultCode.SUCCESS_CREATE_BOOK
                && data != null) {
            setResult(ResultCode.SUCCESS_CREATE_BOOK, data);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.book_selection_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onRefresh();
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter != null) {
            ((BookCardListAdapter) adapter).cancelPictureDownloading();
        }
        super.onBackPressed();
    }
}
