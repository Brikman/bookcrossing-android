package app.bookcrossing.util;


import java.lang.reflect.Field;

public class RequestCode {

    public static int PICK_IMAGE;
    public static int SELECT_BOOK;
    public static int CREATE_BOOK;
    public static int SEND_BOOK;
    public static int TRANSFER_CONFIRM;
    public static int EDIT_BOOK;
    public static int EDIT_PROFILE;
    public static int EDIT_ADDRESS;

    static {
        int value = 1;
        for (Field field : RequestCode.class.getDeclaredFields()) {
            try {
                field.setAccessible(true);
                field.set(null, value++);
            } catch (Exception ignored) {}
        }
    }
}
