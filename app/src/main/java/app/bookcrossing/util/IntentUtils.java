package app.bookcrossing.util;


import android.content.Intent;

import app.bookcrossing.client.net.dto.Address;
import app.bookcrossing.client.net.dto.Book;
import app.bookcrossing.client.net.dto.Transfer;
import app.bookcrossing.client.net.dto.User;
import app.bookcrossing.commons.ExtraKey;

public class IntentUtils {

    public static User getUser(Intent intent) {
        return intent.getParcelableExtra(ExtraKey.USER);
    }

    public static void putUser(User user, Intent intent) {
        if (user == null)
            throw new NullPointerException();
        intent.putExtra(ExtraKey.USER, user);
    }

    public static Book getBook(Intent intent) {
        return intent.getParcelableExtra(ExtraKey.BOOK);
    }

    public static void putBook(Book book, Intent intent) {
        if (book == null)
            throw new NullPointerException();
        intent.putExtra(ExtraKey.BOOK, book);
    }

    public static Transfer getTransfer(Intent intent) {
        return intent.getParcelableExtra(ExtraKey.TRANSFER);
    }

    public static void putTransfer(Transfer transfer, Intent intent) {
        if (transfer == null)
            throw new NullPointerException();
        intent.putExtra(ExtraKey.TRANSFER, transfer);
    }

    public static Address getAddress(Intent intent) {
        return intent.getParcelableExtra(ExtraKey.ADDRESS);
    }

    public static void putAddress(Intent intent, Address address) {
        if (address == null)
            throw new NullPointerException();
        intent.putExtra(ExtraKey.ADDRESS, address);
    }

    public static int getRequestCode(Intent intent) {
        return intent != null ? intent.getIntExtra(ExtraKey.REQUEST_CODE, 0) : 0;
    }

    public static int getResultCode(Intent intent) {
        return intent != null ? intent.getIntExtra(ExtraKey.RESULT_CODE, 0) : 0;
    }
}
