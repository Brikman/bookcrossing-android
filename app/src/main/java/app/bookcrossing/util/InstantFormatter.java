package app.bookcrossing.util;


import org.joda.time.Instant;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class InstantFormatter {

    private static DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");
    private static DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");

    public static String getDate(Instant instant) {
        return dateFormatter.print(instant);
    }

    public static String getDateTime(Instant instant) {
        return dateTimeFormatter.print(instant);
    }
}
