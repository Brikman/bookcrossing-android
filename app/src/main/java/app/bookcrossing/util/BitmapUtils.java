package app.bookcrossing.util;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;

import java.io.ByteArrayOutputStream;

public class BitmapUtils {

    public static final int MAX_DIM = 2048;

    public static Bitmap getScaled(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        if (width > MAX_DIM || height > MAX_DIM) {
            int max = Math.max(width, height);
            float x = 1.0f * max / MAX_DIM;
            return Bitmap.createScaledBitmap(bitmap, (int) (width / x), (int) (height / x), false);
        }
        return bitmap;
    }

    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        return stream.toByteArray();
    }
}
