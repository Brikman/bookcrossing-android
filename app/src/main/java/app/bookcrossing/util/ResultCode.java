package app.bookcrossing.util;


import java.lang.reflect.Field;

public class ResultCode {

    public static int SUCCESS_SELECT_BOOK;
    public static int SUCCESS_CREATE_BOOK;
    public static int SUCCESS_SEND_BOOK;
    public static int SUCCESS_TRANSFER_CONFIRM;
    public static int SUCCESS_EDIT_BOOK;
    public static int SUCCESS_EDIT_PROFILE;
    public static int SUCCESS_EDIT_ADDRESS;

    static {
        int value = 0;
        for (Field field : ResultCode.class.getDeclaredFields()) {
            try {
                field.setAccessible(true);
                field.set(null, value++);
            } catch (Exception ignored) {}
        }
    }
}
