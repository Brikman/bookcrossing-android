package app.bookcrossing.dialog;


import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import app.bookcrossing.R;

@SuppressLint("ValidFragment")
public class MessageDialog extends DialogFragment {

    private String message;

    private TextView text;
    private Button button;

    public MessageDialog(String message) {
        this.message = message;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle state) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.dialog_message, container);
        text = view.findViewById(R.id.dialog_message_text);
        button = view.findViewById(R.id.dialog_message_back);

        text.setText(message);
        button.setOnClickListener(v -> dismiss());
        return view;
    }
}
