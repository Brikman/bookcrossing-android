package app.bookcrossing.dialog;


import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import app.bookcrossing.R;
import app.bookcrossing.activity.EditBookActivity;

@SuppressLint("ValidFragment")
public class AuthorDialog extends DialogFragment {

    private EditBookActivity activity;
    private EditText authorName;
    private Button cancelButton;
    private Button saveButton;
    private int position;

    public AuthorDialog(EditBookActivity activity) {
        this.activity = activity;
        this.position = -1;
    }

    public AuthorDialog(EditBookActivity activity, int position) {
        this.activity = activity;
        this.position = position;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle state) {
        getDialog().setTitle(getString(R.string.add_author));
        View view = inflater.inflate(R.layout.dialog_author, container);
        cancelButton = view.findViewById(R.id.dialog_author_cancel_button);
        cancelButton.setOnClickListener(this::onCancel);
        saveButton = view.findViewById(R.id.dialog_author_save_button);
        saveButton.setOnClickListener(this::onAccept);
        if (position == -1) {
            saveButton.setVisibility(View.INVISIBLE);
        }
        authorName = view.findViewById(R.id.author_name);
        if (position != -1) {
            authorName.setText(activity.getAuthors().get(position));
        }
        authorName.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void afterTextChanged(Editable s) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                saveButton.setVisibility(s.toString().trim().isEmpty() ? View.INVISIBLE : View.VISIBLE);
            }
        });
        return view;
    }

    public void onAccept(View view) {
        String name = authorName.getText().toString();
        if (position == -1) {
            if (activity.addAuthor(name)) {
                super.dismiss();
            }
        } else {
            if (activity.editAuthor(position, name)) {
                super.dismiss();
            }
        }
        authorName.setError("Данный автор уже указан");
    }

    public void onCancel(View view) {
        super.dismiss();
    }
}
