package app.bookcrossing.dialog;


import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import app.bookcrossing.R;

@SuppressLint("ValidFragment")
public class ProgressDialog extends DialogFragment {

    private ProgressBar progressBar;
    private ImageView successView;
    private ImageView errorView;
    private TextView info;
    private View buttonContainer;
    private Button acceptButton;
    private Button declineButton;

    private View.OnClickListener acceptListener;
    private View.OnClickListener cancelListener;

    private Runnable runnable;
    private String progressMessage;
    private boolean showButtons;
    private int delay;

    public ProgressDialog(Runnable runnable, int delay, String progressMessage, boolean showButtons) {
        this.runnable = runnable;
        this.delay = delay;
        this.progressMessage = progressMessage;
        this.showButtons = showButtons;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.dialog_progress, container);

        progressBar = view.findViewById(R.id.dialog_progress_progressbar);
        successView = view.findViewById(R.id.dialog_progress_success);
        errorView = view.findViewById(R.id.dialog_progress_error);

        info = view.findViewById(R.id.dialog_progress_info);

        buttonContainer = view.findViewById(R.id.dialog_progress_button_container);
        if (!showButtons) {
            buttonContainer.setVisibility(View.GONE);
        }
        acceptButton = view.findViewById(R.id.dialog_progress_accept_button);
        if (acceptListener != null)
            acceptButton.setOnClickListener(acceptListener);
        else
            acceptButton.setOnClickListener(v -> dismiss());
        declineButton = view.findViewById(R.id.dialog_progress_cancel_button);
        if (cancelListener != null)
            declineButton.setOnClickListener(cancelListener);
        else
            declineButton.setOnClickListener(v -> dismiss());

        new Handler().postDelayed(runnable, delay);
        showProgress(progressMessage);

        return view;
    }

    public void setOnAcceptListener(View.OnClickListener listener) {
        acceptListener = listener;
    }

    public void setOnDeclineListener(View.OnClickListener listener) {
        cancelListener = listener;
    }

    public void showProgress(String message) {
        info.setText(message);
        buttonContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        successView.setVisibility(View.INVISIBLE);
        errorView.setVisibility(View.INVISIBLE);
    }

    public void showSuccess(String message) {
        getDialog().setCanceledOnTouchOutside(true);
        info.setText(message);
        if (showButtons)
            buttonContainer.setVisibility(View.VISIBLE);
        acceptButton.setVisibility(View.VISIBLE);
        declineButton.setVisibility(View.GONE);
        progressBar.setVisibility(View.INVISIBLE);
        successView.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.INVISIBLE);
    }

    public void showError(String message) {
        getDialog().setCanceledOnTouchOutside(true);
        info.setText(message);
        if (showButtons)
            buttonContainer.setVisibility(View.VISIBLE);
        acceptButton.setVisibility(View.GONE);
        declineButton.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        successView.setVisibility(View.INVISIBLE);
        errorView.setVisibility(View.VISIBLE);
    }
}
