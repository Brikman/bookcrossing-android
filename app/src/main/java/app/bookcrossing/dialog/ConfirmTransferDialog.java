package app.bookcrossing.dialog;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import app.bookcrossing.R;

@SuppressLint("ValidFragment")
public class ConfirmTransferDialog extends DialogFragment {

    private View imageContainer;
    private ProgressBar progressBar;
    private ImageView successView;
    private ImageView errorView;

    private View infoContainer;
    private TextView info;

    private View statusContainer;
    private TextView status;

    private View buttonContainer;
    private Button confirmButton;
    private Button cancelButton;
    private Button successButton;

    private View.OnClickListener onConfirmListener;
    private View.OnClickListener onSuccessListener;

    public void setOnConfirmListener(View.OnClickListener onConfirmListener) {
        this.onConfirmListener = onConfirmListener;
    }

    public void setOnSuccessListener(View.OnClickListener onSuccessListener) {
        this.onSuccessListener = onSuccessListener;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle state) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.dialog_confirm_transfer, container);

        infoContainer = view.findViewById(R.id.dialog_transfer_info_container);
        info = view.findViewById(R.id.dialog_transfer_info);

        statusContainer = view.findViewById(R.id.dialog_transfer_status_container);
        imageContainer = view.findViewById(R.id.dialog_transfer_image_container);
        progressBar = view.findViewById(R.id.dialog_transfer_progress);
        successView = view.findViewById(R.id.dialog_transfer_success);
        errorView = view.findViewById(R.id.dialog_transfer_error);
        status = view.findViewById(R.id.dialog_transfer_status);

        buttonContainer = view.findViewById(R.id.dialog_transfer_button_container);
        cancelButton = view.findViewById(R.id.dialog_transfer_cancel);
        confirmButton = view.findViewById(R.id.dialog_transfer_confirm);
        successButton = view.findViewById(R.id.dialog_transfer_success_button);

        cancelButton.setOnClickListener(v -> dismiss());
        confirmButton.setOnClickListener(this::onConfirm);
        successButton.setOnClickListener(onSuccessListener);

        showInfo();

        return view;
    }

    public void onConfirm(View view) {
        showProgress();
        new Handler().postDelayed(() -> onConfirmListener.onClick(view), 1000);
    }

    public void showInfo() {
        info.setText("Подтвердить получение?");

        infoContainer.setVisibility(View.VISIBLE);
        statusContainer.setVisibility(View.GONE);
        buttonContainer.setVisibility(View.VISIBLE);

        cancelButton.setVisibility(View.VISIBLE);
        confirmButton.setVisibility(View.VISIBLE);
        successButton.setVisibility(View.INVISIBLE);
    }

    public void showProgress() {
        getDialog().setCanceledOnTouchOutside(false);

        infoContainer.setVisibility(View.GONE);
        statusContainer.setVisibility(View.VISIBLE);

        progressBar.setVisibility(View.VISIBLE);
        successView.setVisibility(View.INVISIBLE);
        errorView.setVisibility(View.INVISIBLE);

        buttonContainer.setVisibility(View.GONE);

        status.setText("Подтверждение...");
    }

    public void showSuccess() {
        getDialog().setCanceledOnTouchOutside(true);

        infoContainer.setVisibility(View.GONE);
        statusContainer.setVisibility(View.VISIBLE);

        successView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        errorView.setVisibility(View.INVISIBLE);

        buttonContainer.setVisibility(View.VISIBLE);
        successButton.setVisibility(View.VISIBLE);
        cancelButton.setVisibility(View.INVISIBLE);
        confirmButton.setVisibility(View.INVISIBLE);

        status.setText("Получение успешно подтверждено");
    }

    public void showError() {
        getDialog().setCanceledOnTouchOutside(true);

        infoContainer.setVisibility(View.GONE);
        statusContainer.setVisibility(View.VISIBLE);

        errorView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        successView.setVisibility(View.INVISIBLE);

        buttonContainer.setVisibility(View.GONE);

        status.setText("Ошибка");
    }
}
