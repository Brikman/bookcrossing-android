package app.bookcrossing.dialog;


import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import app.bookcrossing.R;
import app.bookcrossing.activity.RecipientListActivity;
import app.bookcrossing.async.AsyncClientTask;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.net.packet.response.RecipientListResponse;

@SuppressLint("ValidFragment")
public class RecipientDialog extends DialogFragment {

    private RecipientListActivity activity;
    private final int pending;
    private final int available;

    private LinearLayout content;
    private ProgressBar progressBar;
    private Button acceptButton;
    private Button cancelButton;

    private AsyncClientTask<RecipientListResponse> recipientTask;

    public RecipientDialog(RecipientListActivity activity, int pending, int available) {
        this.activity = activity;
        this.pending = pending;
        this.available = available;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle state) {
        getDialog().setTitle(R.string.recipients);
        View view = inflater.inflate(R.layout.dialog_recipient, container);
        content = view.findViewById(R.id.require_recipients_dialog_content);
        progressBar = view.findViewById(R.id.require_recipients_dialog_progress);
        acceptButton = view.findViewById(R.id.dialog_recipients_accept_button);
        cancelButton = view.findViewById(R.id.dialog_recipients_cancel_button);
        acceptButton.setOnClickListener(this::onAccept);
        cancelButton.setOnClickListener(this::onCancel);

        LinearLayout pendingLayout = view.findViewById(R.id.require_recipients_dialog_pending_layout);
        LinearLayout availableLayout = view.findViewById(R.id.require_recipients_dialog_available_layout);
        if (pending == 0) {
            pendingLayout.setVisibility(View.GONE);
            availableLayout.setGravity(Gravity.CENTER);
        } else {
            TextView pendingView = view.findViewById(R.id.require_recipients_dialog_pending_count);
            pendingView.setText(String.valueOf(pending));
        }

        if (available == 0) {
            view.findViewById(R.id.require_recipients_dialog_available_count).setVisibility(View.GONE);
            TextView messageText = view.findViewById(R.id.require_recipients_dialog_message);
            messageText.setText(getString(R.string.no_available_recipients));
            messageText.setTextColor(Color.parseColor("#b91c1c"));
            messageText.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            acceptButton.setVisibility(View.GONE);
            pendingLayout.setGravity(Gravity.CENTER);
        } else {
            TextView availableView = view.findViewById(R.id.require_recipients_dialog_available_count);
            availableView.setText(String.valueOf(available));

        }
        toggleView(false);
        new Handler().postDelayed(() -> toggleView(true), 1500);
        return view;
    }

    public void toggleView(boolean flag) {
        content.setVisibility(flag ? View.VISIBLE : View.INVISIBLE);
        progressBar.setVisibility(!flag ? View.VISIBLE : View.INVISIBLE);
    }

    public void onAccept(View view) {
        toggleView(false);

        Client client = Client.getInstance();
        recipientTask = new AsyncClientTask<RecipientListResponse>()
                .forFuture(client.requireRecipients())
                .onSuccess(response -> activity.onRefresh())
                .onRejected((e) -> activity.onError(e))
                .after(() -> onCancel(view));
        recipientTask.execute();
    }

    public void onCancel(View view) {
        if (recipientTask != null) {
            recipientTask.cancel(true);
        }
        super.dismiss();
    }
}
