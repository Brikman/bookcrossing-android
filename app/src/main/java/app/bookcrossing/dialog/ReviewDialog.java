package app.bookcrossing.dialog;


import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import app.bookcrossing.R;
import app.bookcrossing.client.net.dto.Review;
import app.bookcrossing.client.net.dto.Transfer;

public class ReviewDialog extends DialogFragment {

    private LinearLayout ratingContainer;
    private EditText comment;
    private TextView charsRemains;

    private Button cancel;
    private Button send;

    private int rating;
    private List<ImageView> ratingStars = new ArrayList<>();

    private OnSendReviewListener listener;

    public void setOnSendReviewListener(OnSendReviewListener listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.dialog_review, container);

        ratingContainer = view.findViewById(R.id.review_rating_container);
        charsRemains = view.findViewById(R.id.review_comment_chars_remain);
        charsRemains.setText(String.format(Locale.US,
                "Осталось %d/%d", Review.COMMENT_MAX_LENGTH, Review.COMMENT_MAX_LENGTH));
        comment = view.findViewById(R.id.review_comment);
        comment.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void afterTextChanged(Editable s) {
                if (s.length() > Transfer.MESSAGE_MAX_LENGTH) {
                    s.delete(Transfer.MESSAGE_MAX_LENGTH, s.length());
                }
            }
            @SuppressLint("DefaultLocale")
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                charsRemains.setText(String.format("Осталось %d/%d",
                        Transfer.MESSAGE_MAX_LENGTH - s.length(), Transfer.MESSAGE_MAX_LENGTH));
            }
        });

        cancel = view.findViewById(R.id.review_cancel);
        send = view.findViewById(R.id.review_send);
        send.setVisibility(View.GONE);
        cancel.setOnClickListener(v -> dismiss());
        send.setOnClickListener(v -> {
            if (listener != null) {
                listener.send(comment.getText().toString().trim(), rating);
            }
        });

        for (int i = 1; i <= Review.MAX_RATING; i++) {
            send.setVisibility(View.VISIBLE);
            final int rating = i;
            ImageView star = new ImageView(getActivity());
            star.setLayoutParams(new LinearLayout.LayoutParams(100, 100, 1));
            star.setOnClickListener(v -> setRating(rating));
            ratingStars.add(star);
            ratingContainer.addView(star);
        }

        setRating(0);

        return view;
    }

    private void setRating(int rating) {
        this.rating = rating;
        for (ImageView star : ratingStars) {
            star.setImageResource(R.drawable.star_border_blue);
        }
        for (int i = 0; i < rating; i++) {
            ratingStars.get(i).setImageResource(R.drawable.star_blue);
        }
    }

    public void onSendReview(View view) {

    }

    public interface OnSendReviewListener {

        void send(String comment, int rating);
    }
}
