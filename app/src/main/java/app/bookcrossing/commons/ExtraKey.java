package app.bookcrossing.commons;


import java.lang.reflect.Field;

public class ExtraKey {

    public static String REQUEST_CODE;
    public static String RESULT_CODE;

    public static String USER;
    public static String USER_ID;
    public static String RECIPIENT;
    public static String RECIPIENT_ID;
    public static String BOOK;
    public static String BOOK_ID;
    public static String BOOK_EDITABLE;
    public static String AUTHOR;
    public static String AUTHOR_ID;
    public static String TRANSFER;
    public static String TRANSFER_ID;
    public static String ADDRESS;
    public static String TRANSFER_VIEW;
    public static String IMAGE_URI;
    public static String EDIT_BOOK_MODE;

    static {
        for (Field field : ExtraKey.class.getDeclaredFields()) {
            try {
                String value = field.getName().toLowerCase();
                field.setAccessible(true);
                field.set(null, value);
            } catch (Exception ignored) {}
        }
    }
}
