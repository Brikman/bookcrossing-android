package app.bookcrossing.logging;


public class TagHelper {

    public static String getTag() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        for (int i = 1; i < stackTrace.length; i++) {
            String className = stackTrace[i].getClassName();
            if (!className.equals(TagHelper.class.getName())
                    && className.indexOf("java.lang.Thread") != 0) {
                return "BC_" + className.substring(className.lastIndexOf(".") + 1);
            }
        }
        return null;
    }
}
