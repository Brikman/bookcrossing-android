package app.bookcrossing.async.callback;

@FunctionalInterface
public interface Producer<P, R> {
    R execute(P params) throws Exception;
}
