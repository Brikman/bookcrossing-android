package app.bookcrossing.async.callback;

@FunctionalInterface
public interface Consumer<P> {
    void execute(P params) throws Exception;
}
