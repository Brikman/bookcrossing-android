package app.bookcrossing.async.callback;

@FunctionalInterface
public interface Action {
    void execute() throws Exception;
}
