package app.bookcrossing.async;


import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.DrawableRes;
import android.widget.ImageView;

import app.bookcrossing.async.callback.Action;
import app.bookcrossing.async.callback.Consumer;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.future.ResponseFuture;
import app.bookcrossing.client.net.dto.Entity;
import app.bookcrossing.client.net.packet.response.PictureResponse;
import app.bookcrossing.util.BitmapUtils;

public class PictureTask extends AsyncTask<Void, Void, PictureResponse> {

    private Entity entity;
    private ImageView image;
    private ResponseFuture<PictureResponse> future;
    private Consumer<PictureResponse> successCallback;
    private Action failedCallback;
    private CallbackExecutor executor = new CallbackExecutor();

    private int defaultPictureId;

    public PictureTask(Entity entity, ImageView image, @DrawableRes int defaultPictureId) {
        this.entity = entity;
        this.image = image;
        this.defaultPictureId = defaultPictureId;
    }

    public PictureTask onSuccess(Consumer<PictureResponse> callback) {
        this.successCallback = callback;
        return this;
    }

    public PictureTask onFailed(Action callback) {
        this.failedCallback = callback;
        return this;
    }

    @Override
    protected void onPreExecute() {
        setDefault();
    }

    @Override
    protected void onPostExecute(PictureResponse response) {
        if (response != null) {
            executor.execute(successCallback, response);
            try {
                Bitmap bitmap = response.getPicture().getBitmap();
                image.setImageBitmap(BitmapUtils.getScaled(bitmap));
            } catch (Exception ignored) {
                setDefault();
            }
        } else {
            executor.execute(failedCallback);
            setDefault();
        }
    }

    private void setDefault() {
        image.setImageResource(defaultPictureId);
    }

    @Override
    protected PictureResponse doInBackground(Void... voids) {
        try {
            Client client = Client.getInstance();
            future = client.getPicture(entity);
            return future.get();
        } catch (Exception ignored) {}
        return null;
    }

    @Override
    protected void onCancelled() {
        future.cancel();
    }
}
