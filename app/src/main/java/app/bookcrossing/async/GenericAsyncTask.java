package app.bookcrossing.async;

import android.os.AsyncTask;
import android.util.Log;

import java.util.Objects;

import app.bookcrossing.async.callback.Action;
import app.bookcrossing.async.callback.Consumer;
import app.bookcrossing.async.callback.Producer;
import app.bookcrossing.logging.TagHelper;

public class GenericAsyncTask<P, R>
        extends AsyncTask<P, Void, R> {

    private static final String TAG = TagHelper.getTag();

    protected CallbackExecutor executor = new CallbackExecutor();

    protected Action beforeAction;
    protected Action afterAction;
    protected Producer<P, R> action;
    protected Consumer<? super R> onSuccess;
    protected Consumer<? super Exception> onException;

    private boolean success;
    private Exception exception;

    public GenericAsyncTask<P, R> before(Action action) {
        this.beforeAction = action;
        return this;
    }

    public GenericAsyncTask<P, R> forAction(Producer<P, R> action) {
        this.action = Objects.requireNonNull(action);
        return this;
    }

    public GenericAsyncTask<P, R> onSuccess(Consumer<? super R> callback) {
        this.onSuccess = callback;
        return this;
    }

    public GenericAsyncTask<P, R> onException(Consumer<? super Exception> callback) {
        this.onException = callback;
        return this;
    }

    public GenericAsyncTask<P, R> after(Action callback) {
        afterAction = callback;
        return this;
    }

    @Override
    protected void onPreExecute() {
        executor.execute(beforeAction);
    }

    @Override
    protected void onPostExecute(R result) {
        if (success) {
            executor.execute(onSuccess, result);
        } else {
            executor.execute(onException, exception);
        }
        executor.execute(afterAction);
    }

    @Override
    protected R doInBackground(P... params) {
        success = false;
        try {
            if (action != null) {
                R result = action.execute(params.length > 0 ? params[0] : null);
                success = true;
                return result;
            }
        } catch (Exception e) {
            exception = e;
        }
        return null;
    }
}
