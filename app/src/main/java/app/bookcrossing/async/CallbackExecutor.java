package app.bookcrossing.async;


import android.util.Log;

import app.bookcrossing.async.callback.Action;
import app.bookcrossing.async.callback.Consumer;
import app.bookcrossing.logging.TagHelper;

class CallbackExecutor {

    private static final String TAG = TagHelper.getTag();

    public boolean execute(Action action) {
        if (action == null)
            return false;
        try {
            action.execute();
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Callback execution error", e);
            return false;
        }
    }

    public <T> boolean execute(Consumer<T> consumer, T param) {
        if (consumer == null)
            return false;
        try {
            consumer.execute(param);
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Callback execution error", e);
            return false;
        }
    }
}
