package app.bookcrossing.async;


import android.os.AsyncTask;
import android.util.Log;

import java.util.Objects;

import app.bookcrossing.async.callback.Action;
import app.bookcrossing.async.callback.Consumer;
import app.bookcrossing.client.core.Client;
import app.bookcrossing.client.future.RejectionException;
import app.bookcrossing.client.future.ResponseFuture;
import app.bookcrossing.client.net.packet.response.Response;
import app.bookcrossing.logging.TagHelper;

public class AsyncClientTask<R extends Response>
        extends AsyncTask<ResponseFuture<R>, Void, R> {

    private static final String TAG = TagHelper.getTag();

    protected CallbackExecutor executor = new CallbackExecutor();

    protected Action beforeAction;
    protected ResponseFuture<R> future;
    protected Consumer<? super R> onSuccess;
    protected Consumer<? super RejectionException> onRejection;
    protected Consumer<? super Exception> onException;
    protected Action inFinally;

    private boolean success;
    private Exception exception;

    public AsyncClientTask<R> before(Action action) {
        this.beforeAction = action;
        return this;
    }

    public AsyncClientTask<R> after(Action action) {
        inFinally = action;
        return this;
    }

    public AsyncClientTask<R> forFuture(ResponseFuture<R> future) {
        this.future = Objects.requireNonNull(future);
        return this;
    }

    public AsyncClientTask<R> onSuccess(Consumer<? super R> callback) {
        onSuccess = callback;
        return this;
    }

    public AsyncClientTask<R> onRejected(Consumer<? super RejectionException> callback) {
        onRejection = callback;
        return this;
    }

    public AsyncClientTask<R> onException(Consumer<? super Exception> callback) {
        onException = callback;
        return this;
    }

    @Override
    protected void onPreExecute() {
        executor.execute(beforeAction);
    }

    @Override
    protected void onPostExecute(R result) {
        if (success) {
            executor.execute(onSuccess, result);
        } else {
            if (exception instanceof RejectionException) {
                executor.execute(onRejection, (RejectionException) exception);
            } else {
                executor.execute(onException, exception);
            }
        }
        executor.execute(inFinally);
    }

    @Override
    protected R doInBackground(ResponseFuture<R>... futures) {
        success = false;
        if (future != null) {
            try {
                R result = future.get();
                success = true;
                return result;
            } catch (Exception e) {
                exception = e;
            }
        }
        return null;
    }

    @Override
    protected void onCancelled() {
        future.cancel();
    }
}
