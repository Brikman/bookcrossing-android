package app.bookcrossing.async;


public interface FutureHolder {

    void cancelFutures();
}
